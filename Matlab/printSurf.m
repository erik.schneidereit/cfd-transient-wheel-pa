function printSurf(xx, yy, xxx, yyy, z, j, k, yMin, yMax, zMin, zMax, outputPath, show, save, quiv, valMin, valMax, label, vorticity)
%This funciton creates the Surface Plot for the propper Printer

%Make show into the right parameter
if show ==0
    showIt = 'off';
else
    showIt = 'on';
end

if vorticity == 5
        plot = contourf(xx,yy, z);
        set(gcf, 'Visible', showIt); %this defines if the plot will be visible prior saaving. 
        colormap jet
        shading flat
        %view(0,90)
        colorbar
        axis([yMin yMax zMin zMax])
        caxis([valMin valMax])
        set(gcf, 'Position',  [50, 50, 1200, 1200]) %Letze beiden stellen waren 1200 => Fenstergr��e ge�ndert wegen kleinem Bildschirm / 900 f�r X220
        axis('square')  
        xlabel(label.xAxisLabel) 
        ylabel(label.yAxisLabel)
        t=title(label.title);
        set(gca,'FontSize',25)
        t.FontSize = 35;
    
    
else
    if quiv == 1

    %Export coloured contour plot
    plot = contourf(xx,yy, z);
        set(gcf, 'Visible', showIt); %this defines if the plot will be visible prior saaving. 
        colormap jet
        shading flat
        %view(0,90)
        colorbar
        axis([yMin yMax zMin zMax])
        caxis([valMin valMax])
        set(gcf, 'Position',  [50, 50, 1200, 1200]) %Letze beiden stellen waren 1200 => Fenstergr��e ge�ndert wegen kleinem Bildschirm / 900 f�r X220
        axis('square')  
        xlabel(label.xAxisLabel) 
        ylabel(label.yAxisLabel)
        t=title(label.title);
        set(gca,'FontSize',25)
        t.FontSize = 35;
        hold on
            quiver(xxx,yyy,j,k, 'black')
        hold off 
    else

        plot = surf(xx,yy, z);
            set(gcf, 'Visible', showIt); %this defines if the plot will be visible prior saaving. 
            colormap jet
            shading flat
            view(0,90)
            colorbar
            axis([yMin yMax zMin zMax])
            caxis([valMin valMax])
            xlabel(label.xAxisLabel) 
            ylabel(label.yAxisLabel)
            t=title(label.title);
            set(gca,'FontSize',25)
            t.FontSize = 35;
            set(gcf, 'Position',  [50, 50, 1200, 1200]) %Letze beiden stellen waren 1200 => Fenstergr��e ge�ndert wegen kleinem Bildschirm / 900 f�r X220
            axis('square')  

    end

end


if save == 1
    saveas(gcf, outputPath)
end
    
end