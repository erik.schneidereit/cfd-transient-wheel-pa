function outp = propperCalculator(averaged, interpolation, path, outputDirectory, files, surfacePlot, quiverPlot, column, cor, numFiles)
%does the calcualtion part for the propper printer gives matrix back which
%can be printed in the next step
%   Detailed explanation goes here

    xx = cor.xx;
    yy = cor.yy;
    xxx = cor.xxx;
    yyy = cor.yyy;
    
    for v = 1:numFiles
        
        disp("Calculating: " + v + "/" + numFiles) %Shows data collection progress, to be changed to a progress bar in the future. 
        
        %chose current file
        currentFile = files(v).name;


        %Importing Plane Data, removinv invalid and duplicat data
        startData = importdata(path + currentFile);
        cleanData = startData(1).data( ~any( isnan( startData(1).data ) | isinf( startData(1).data ), 2 ),: );
        cleanDataUnique = unique(cleanData,'rows');

        %Creatin Interpolant Object depending on the configuration. 
        
        interpolantI = scatteredInterpolant(cleanDataUnique(:,column.X), cleanDataUnique(:,column.Y), cleanDataUnique(:,column.Vi), interpolation);
        

        if quiverPlot == 1
            interpolantJ = scatteredInterpolant(cleanDataUnique(:,column.X), cleanDataUnique(:,column.Y),cleanDataUnique(:,column.Vj), interpolation);
            interpolantK = scatteredInterpolant(cleanDataUnique(:,column.X), cleanDataUnique(:,column.Y),cleanDataUnique(:,column.Vk), interpolation);
        end

        %Averageing Data if desired or directly print every single data set. 
        if averaged == 1

            if v == 1
                outp(1).sumDataI = interpolantI(xx,yy);
                if quiverPlot == 1
                    outp(1).sumDataJ = interpolantJ(xxx,yyy);
                    outp(1).sumDataK = interpolantK(xxx,yyy);
                end
            else
                outp(1).sumDataI = outp(1).sumDataI + interpolantI(xx,yy);
                if quiverPlot == 1
                    outp(1).sumDataJ = outp(1).sumDataJ + interpolantJ(xxx,yyy);
                    outp(1).sumDataK = outp(1).sumDataK + interpolantK(xxx,yyy);
                end
            end
    

            
        elseif averaged == 0
            outp(v).I = interpolantI(xx,yy);
            outp(v).path = outputDirectory + files(v).name +".jpg";
            if quiverPlot == 1
                    outp(v).J = interpolantJ(xxx,yyy);
                    outp(v).K = interpolantK(xxx,yyy);
                    outp(v).path = outputDirectory + files(v).name +"_quiv.jpg";
            end
            
        end
    end
    
    
    %Deviding as final step of averaging all data samples. 
    if averaged == 1
        outp.I = outp(1).sumDataI / numFiles;
        if quiverPlot == 1
            outp.J = outp(1).sumDataJ / numFiles;
        	outp.K = outp(1).sumDataK / numFiles;
        end
    end
end
    

