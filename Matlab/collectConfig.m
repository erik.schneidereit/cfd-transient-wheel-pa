function [output] = collectConfig(path)
%COLLECTCONFIG collects data from config file and parses it. 
% 

%Open config file embedded in the Folder where Data will be imported
configFile = fopen(path);

%read information from config file
theData = textscan(configFile, '%s %s', 'Delimiter', '=', 'CommentStyle', '#');
theNames = theData{1,1};
theValues = theData{1,2};

theSize = size(theNames);

%compile information from config file into a struct, for easy handling
%later in the main propper printer
    for n = 1:theSize(1)
        name = char(theNames(n,1));
        output.(name) = theValues(n);


    end


end

