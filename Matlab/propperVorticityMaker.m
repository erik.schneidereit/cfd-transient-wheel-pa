function [data] = propperVorticityMaker(data, cor, quivResolution)
%PROPPERVORTICITYMAKER calculates Vorticity from Velocity matrix. 
%   Detailed explanation goes here
	
	xxx = cor.xxx + 0.5 * quivResolution;
	xxx = xxx(:,1:end-1);
	yyyA = cor.yyy(:,1:end-1);
	yyy = cor.yyy + 0.5 * quivResolution;
	yyy = yyy(1:end-1,:);
	xxxB = cor.xxx(2:end,:);
	
	sizeOutput = size(data);
	sizecor = size(cor.xxx);

	for n = 1:sizeOutput(2)
	
		a = diff(data(n).K,1,2);   %rot = a - b
		b = diff(data(n).J);
	
		rotA = scatteredInterpolant(xxx(:), yyyA(:),a(:),'natural');
		rotB = scatteredInterpolant(xxxB(:), yyy(:),b(:),'natural');
		
		data(n).I = -((rotA(cor.xxx,cor.yyy) - rotB(cor.xxx,cor.yyy)) / quivResolution);
		
	end
	
	
	
	
end

