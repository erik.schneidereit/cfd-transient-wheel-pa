%---------SETUP-----------
%Debug mode: Skips all form of UI Input that a full run of the code can be done without any necessary user intervention
dbug = 0; %1 means debut mode is on, 0 means debug mode is of. 
bulk = 0; %for bulk importing and doing stuff.
%useConfigReader = 0;    This should do something in the future!!!

if bulk == 0
    j = 1;
    pathListEntry = 0;
    [baseName, folder] = uigetfile('*.txt');
    configPath = fullfile(folder, baseName);
    disp(configPath)
else
    [pathList, configPath] = pathListCollect();
    j = 4;
end


%configPath = 'propperConfig_Vorlage.txt';

config = collectConfig(configPath);     %This might get change for taking the config file from the folder in question. 

for a = 1:j

    %preset if wind tunnel data (already averaged, or has to be averaged)
    averaged = str2double(config.averaged);			%1 will average all data samples into one plot.
    surfacePlot = str2double(config.surfacePlot);		%1 if surface plot should be created. 
    quiverPlot = str2double(config.quiverPlot);			%1 if quiver plot should be created. 
    showPlot = str2double(config.showPlot);       %'off' if plot should not be shown, before saving)
    savePlot = str2double(config.savePlot);           %1 if Plot should be saved, after calculating and maybe showing it.
    difference = str2double(config.difference);		% 1 if diffence Plot between two graphs should be done. 
    vorticity = str2double(config.vorticity); 		%1 if vorticity should be plotted instead of velocity.
    interpolation = char(config.interpolation);

    %dataPresets
    column.X = str2double(config.columnX);
    column.Y = str2double(config.columnY);
    column.Vi = str2double(config.columnVi);
    column.Vj = str2double(config.columnVj);
    column.Vk = str2double(config.columnVk);
    column.VMagnitude = str2double(config.columnVMagnitude);

    %Rndering pre-sets Later to be added to GUI but with using presets. 
    resolution = str2double(config.resolution);
    quivResolution = str2double(config.quivResolution);
    yMin = str2double(config.yMin);
    yMax = str2double(config.yMax);
    zMin = str2double(config.zMin);
    zMax = zMin + (yMax - yMin);
    valueMin = str2double(config.valueMin);
    valueMax = str2double(config.valueMax); 
    label.xAxisLabel = config.xAxisLabel;
    label.yAxisLabel = config.yAxisLabel;
    label.title = config.title;

    %path and file type information
    fileType = config.fileType;             %defines file type to search for later
    plane = config.plane;                    %defines what planes to look for in the given naming convention
    
    if bulk == 1
        pathListEntry = pathList(a);
        disp(pathListEntry)
    end
    
    [fileInfo, files] = propperFileSearch(plane, fileType, bulk, pathListEntry);
    
    if difference == 1
        [fileInfoDiff, filesDiff] = propperFileSearch(plane, fileType, bulk, pathListEntry);
    end


    %checks if there is already an output directory in the data set folder and
    %creates on if there isn't and saving is switched on. 
    if and(~exist(fileInfo.outputDirectory, 'dir'),savePlot == 1)
           mkdir(fileInfo.outputDirectory)
    end

    %creates grid matrix used during the interpolation process
    [cor.xx,cor.yy] = meshgrid(yMin:resolution:yMax,zMin:resolution:zMax); 
    [cor.xxx,cor.yyy] = meshgrid(yMin:quivResolution:yMax,zMin:quivResolution:zMax); 

    %Calling the calculater wich gives back the elements to print depending on
    %settings in th config. 
    output = propperCalculator(averaged, interpolation, fileInfo.path, fileInfo.outputDirectory,files, surfacePlot, quiverPlot, column, cor, fileInfo.numFiles);
    if difference == 1
        outputDiff = propperCalculator(averaged, interpolation, fileInfoDiff.path, fileInfo.outputDirectory,filesDiff, surfacePlot, quiverPlot, column, cor, fileInfoDiff.numFiles);
    end

    if vorticity == 1
        quiverPlot = 0;
        output = propperVorticityMaker(output, cor, quivResolution)
    end


    %This builds the file names for the printed file if an average is done to distinguish it from others. 
    if averaged == 1
        folderNameLast = regexp(fileInfo.path,'\','split'); 
        folderName = erase(folderNameLast(end),"/");
        if vorticity == 1
           vortPath = "vorticity";
        else
           vortPath = "";
        end
        
        output(1).path = fileInfo.outputDirectory + folderName + plane + vortPath +".jpg";
        if difference == 1
            folderNameLastDiff = regexp(fileInfoDiff.path,'\','split'); 
            folderNameDiff = erase(folderNameLastDiff(end),"/");
            output(1).path = fileInfo.outputDirectory + folderName + "DIFF" + folderNameDiff +".jpg";  %adds the information that a difference is done to the file name
        end
    end

    %plotting and saving everything. in case of difference: Difference between choosen samples gets created here. 
    sizeOutput = size(output);

    for n = 1:sizeOutput(2)
        disp("Printing: " + n +"/" + sizeOutput(2))

        if difference == 1
            output(n).I = output(n).I - outputDiff(n).I;
            if quiverPlot ==1
                output(n).J = output(n).J - outputDiff(n).J;
                output(n).K = output(n).K - outputDiff(n).K;
            end
        end

        if quiverPlot == 1
            printSurf(cor.xx, cor.yy, cor.xxx, cor.yyy,output(n).I, output(n).J, output(n).K, yMin, yMax, zMin, zMax, output(n).path, showPlot, savePlot, quiverPlot, valueMin, valueMax, label, vorticity)
        else
            printSurf(cor.xx, cor.yy, cor.xxx, cor.yyy,output(n).I, 0, 0, yMin, yMax, zMin, zMax, output(n).path, showPlot, savePlot, quiverPlot, valueMin, valueMax, label, vorticity)
        end
    end

end