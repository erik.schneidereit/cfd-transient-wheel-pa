// STAR-CCM+ macro: ImportFields.java
// Written by STAR-CCM+ 12.02.011
package macro;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import star.turbulence.*;
import star.keturb.*;
import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.post.*;
import star.vis.*;

public class ImportFields extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

	Simulation simulation_0 = getActiveSimulation();
	PhysicsContinuum physicsContinuum_0 = ((PhysicsContinuum) simulation_0.getContinuumManager().getContinuum("Physics 1"));

    // GET IMPORT TABLE
    String simulation_name = simulation_0.getPresentationName();
    simulation_name = simulation_name.endsWith("_t") ? simulation_name.substring(0, simulation_name.length()-2) : simulation_name;
    simulation_name = simulation_name.endsWith("_s") ? simulation_name.substring(0, simulation_name.length()-2) : simulation_name;
    FileTable import_table = 
      (FileTable) simulation_0.getTableManager().createFromFile(resolvePath(simulation_0.getSessionDir()+File.separator+simulation_name+".csv"));

    simulation_0.println("Imported Table!");

    // SET PRESSURE
    InitialPressureProfile initialPressureProfile_0 = 
      physicsContinuum_0.getInitialConditions().get(InitialPressureProfile.class);
    initialPressureProfile_0.setMethod(XyzTabularScalarProfileMethod.class);
    initialPressureProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
    initialPressureProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Pressure");

    // SPALART ALLMARAS
    try {
        SpalartAllmarasTurbulence spalartAllmarasTurbulence_0 = 
            physicsContinuum_0.getModelManager().getModel(SpalartAllmarasTurbulence.class);

        physicsContinuum_0.getInitialConditions().get(SaTurbSpecOption.class).setSelected(SaTurbSpecOption.Type.MODIFIED_TURBULENT_DIFFUSIVITY);

        SaTurbDiffusivityProfile saTurbDiffusivityProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(SaTurbDiffusivityProfile.class);
        saTurbDiffusivityProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
        saTurbDiffusivityProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Modified Diffusivity");

    saTurbDiffusivityProfile_0.setMethod(XyzTabularScalarProfileMethod.class);

    } catch (Exception ex6) {}

    // KW
    try {
        KwAllYplusWallTreatment kwAllYplusWallTreatment_0 = 
            physicsContinuum_0.getModelManager().getModel(KwAllYplusWallTreatment.class);

        physicsContinuum_0.getInitialConditions().get(KwTurbSpecOption.class).setSelected(KwTurbSpecOption.Type.K_OMEGA);

        SpecificDissipationRateProfile specificDissipationRateProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(SpecificDissipationRateProfile.class);
        specificDissipationRateProfile_0.setMethod(XyzTabularScalarProfileMethod.class);
        specificDissipationRateProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
        specificDissipationRateProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Specific Dissipation Rate");

        TurbulentKineticEnergyProfile turbulentKineticEnergyProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(TurbulentKineticEnergyProfile.class);
        turbulentKineticEnergyProfile_0.setMethod(XyzTabularScalarProfileMethod.class);
        turbulentKineticEnergyProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
        turbulentKineticEnergyProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Turbulent Kinetic Energy");
    } catch (Exception ex7) {}

    // KE
    try {
        KEpsilonTurbulence kEpsilonTurbulence_1 = 
            physicsContinuum_0.getModelManager().getModel(KEpsilonTurbulence.class);

        physicsContinuum_0.getInitialConditions().get(KeTurbSpecOption.class).setSelected(KeTurbSpecOption.Type.K_EPSILON);

        TurbulentDissipationRateProfile turbulentDissipationRateProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(TurbulentDissipationRateProfile.class);
        turbulentDissipationRateProfile_0.setMethod(XyzTabularScalarProfileMethod.class);
        turbulentDissipationRateProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
        turbulentDissipationRateProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Turbulent Dissipation Rate");

        TurbulentKineticEnergyProfile turbulentKineticEnergyProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(TurbulentKineticEnergyProfile.class);
        turbulentKineticEnergyProfile_0.setMethod(XyzTabularScalarProfileMethod.class);
        turbulentKineticEnergyProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
        turbulentKineticEnergyProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Turbulent Kinetic Energy");

    } catch (Exception ex8) {}

    // VELOCITY
    VelocityProfile velocityProfile_0 = 
      physicsContinuum_0.getInitialConditions().get(VelocityProfile.class);
    velocityProfile_0.setMethod(XyzTabularVectorProfileMethod.class);
    velocityProfile_0.getMethod(XyzTabularVectorProfileMethod.class).setTable(import_table);
    velocityProfile_0.getMethod(XyzTabularVectorProfileMethod.class).setXData("Velocity[i]");
    velocityProfile_0.getMethod(XyzTabularVectorProfileMethod.class).setYData("Velocity[j]");
    velocityProfile_0.getMethod(XyzTabularVectorProfileMethod.class).setZData("Velocity[k]");

    // CLEAR SOLUTION
    Solution solution_0 = 
      simulation_0.getSolution();
    solution_0.clearSolution(Solution.Clear.History, Solution.Clear.Fields);

    // INITIALISE SOLUTION
    solution_0.initializeSolution();
  }
}
