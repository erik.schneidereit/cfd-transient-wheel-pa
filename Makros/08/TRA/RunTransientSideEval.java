package macro;

import java.util.*;
import java.util.regex.*;
import java.util.Arrays;
import java.io.*;
import java.text.DecimalFormat;

import star.turbulence.*;
import star.kwturb.*;
import star.keturb.*;
import star.saturb.*;
import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.base.report.*;
import star.post.*;
import star.vis.*;
import star.coupledflow.*;

public class RunTransientSideEval extends StarMacro {

    private Simulation simulation_0;
    private PhysicsContinuum physicsContinuum_0;
    private boolean _order = false;
    private double _max_time = 1.0;
    private double _eval_time = 0.05;
    private double _time_step = 0.01;
    private double _maxCFL = 5.0, _meanCFL = 0.5, _pseudoCFL=50.0;
    private boolean _useCFL = false;
    private int _update_freq = 10, _inner_iterations = 5, _max_steps = 2000000;
    private String _settings = "Transient.settings";
    private String _macro = "EvalParse.java";
    private String _MSTRING = "RunTransient: ";

    public void execute() {
        simulation_0 = getActiveSimulation();
        physicsContinuum_0 = ((PhysicsContinuum) simulation_0.getContinuumManager().getContinuum("Physics 1"));
        auto_run();
    }

    private void auto_run()  {
        simulation_0.println("Performing automated run..");
        int max_step = ((int) (_max_time / _time_step));

        // GET SETTINGS
        ImplicitUnsteadySolver implicitUnsteadySolver_0 =
            ((ImplicitUnsteadySolver) simulation_0.getSolverManager().getSolver(ImplicitUnsteadySolver.class));
        InnerIterationStoppingCriterion innerIterationStoppingCriterion_0 =
            ((InnerIterationStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Maximum Inner Iterations"));
        StepStoppingCriterion stepStoppingCriterion_0 = 
          ((StepStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Maximum Steps"));
        AbortFileStoppingCriterion abortFileStoppingCriterion_0 = 
          ((AbortFileStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Stop File"));
        abortFileStoppingCriterion_0.setIsUsed(true);


        for (int step = 1; step <= max_step; step++) {
            long startTime = System.currentTimeMillis();
            parse();

            // SET SETTINGS 
            implicitUnsteadySolver_0.getTimeStep().setValue(_time_step);
            innerIterationStoppingCriterion_0.setMaximumNumberInnerIterations(_inner_iterations);
            stepStoppingCriterion_0.setMaximumNumberSteps(_max_steps);

            if (_order == true) {
                implicitUnsteadySolver_0.getTimeDiscretizationOption().setSelected(TimeDiscretizationOption.Type.SECOND_ORDER);
            } else {
                implicitUnsteadySolver_0.getTimeDiscretizationOption().setSelected(TimeDiscretizationOption.Type.FIRST_ORDER);
            }

            if (_useCFL == true) {
                try {
                    physicsContinuum_0.enable(ConvectiveCflTimeStepControlModel.class);

                    TimeStepControlSolver timeStepControlSolver_0 = 
                      ((TimeStepControlSolver) simulation_0.getSolverManager().getSolver(TimeStepControlSolver.class));
                    ConvectiveCflTimeStepControlSolver convectiveCflTimeStepControlSolver_0 = 
                      ((ConvectiveCflTimeStepControlSolver)
                       ((TimeStepControlSubSolverManager) timeStepControlSolver_0.getTimeStepControlSubSolverManager()).getSolver(_MSTRING + "Convective CFL Time-Step Control"));

                    timeStepControlSolver_0.setVerbose(true);
                    timeStepControlSolver_0.setMaxTimeStepChangeFactor(2.0);
                    ((ScalarPhysicalQuantity) timeStepControlSolver_0.getMinTimeStep()).setValue(1.0E-6);
                    ((ScalarPhysicalQuantity) convectiveCflTimeStepControlSolver_0.getTargetMeanCfl()).setValue(_meanCFL);
                    ((ScalarPhysicalQuantity) convectiveCflTimeStepControlSolver_0.getTargetMaxCfl()).setValue(_maxCFL);
                } catch (Exception CFLoffEx) {simulation_0.println(_MSTRING + "Faild to set Convective CFL Time-Step Control");}
            } else {
                try {
                    ConvectiveCflTimeStepControlModel convectiveCflTimeStepControlModel_0 = 
                      physicsContinuum_0.getModelManager().getModel(ConvectiveCflTimeStepControlModel.class);

                    physicsContinuum_0.disableModel(convectiveCflTimeStepControlModel_0);
                } catch (Exception CFLoffEx) {simulation_0.println(_MSTRING + "Faild to disable Convective CFL Time-Step Control");}
            }

            try {
            CoupledImplicitSolver coupledImplicitSolver_0 = 
              ((CoupledImplicitSolver) simulation_0.getSolverManager().getSolver(CoupledImplicitSolver.class));

            coupledImplicitSolver_0.setCFL(10.0);
            } catch (Exception CoupledSetEx) {simulation_0.println(_MSTRING + "Faild to set Coupled Solver's pseudo time CFL number");}

            long endTime = System.currentTimeMillis();
            double timeTaken =  ((double)(endTime - startTime))/1000;
            simulation_0.println(_MSTRING + "Time used for reading settings file and updating simulation: " + timeTaken);

            // STEP
            simulation_0.getSimulationIterator().step(_update_freq);

            simulation_0.println(_MSTRING + "Testing for eval execution...");
            if (simulation_0.getSolution().getPhysicalTime() >= _eval_time)
            {

                double physicalTime = simulation_0.getSolution().getPhysicalTime();
                String exportString = simulation_0.getPresentationName() + "_" + physicalTime + ".sim";
                simulation_0.saveState(simulation_0.getSessionDir() + File.separator + exportString);

                try
                {
                    String cmd = "cp EvalAndRemove.slurm.sh EvalAndRemove_" + physicalTime + ".slurm.sh";
                    Process p = Runtime.getRuntime().exec(cmd);
                    int result = p.waitFor();
                } catch (Exception runex) {}

                try (
                    FileWriter fw = new FileWriter("EvalAndRemove_" + physicalTime + ".slurm.sh", true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    PrintWriter out = new PrintWriter(bw)
                )
                {
                    out.println("starccm+ -batch -mpi intel -machinefile machinefile_${SLURM_JOB_ID} -rsh ssh ./EvalParse.java " + exportString + " -np 4 -licpath 1999@flex.cd-adapco.com -power -podkey LCuETktEcaVpDEtlvLVJOw");
                    out.println("rm " + exportString + " " + exportString + "~");
                } catch (IOException ioex) {}
                try
                {
                    String cmd = "sbatch EvalAndRemove_" + physicalTime + ".slurm.sh";
                    Process p = Runtime.getRuntime().exec(cmd);
                    int result = p.waitFor();
                } catch (Exception runex) {}
            }


            simulation_0.println(_MSTRING + "Testing for maximum time...");
            if (simulation_0.getSolution().getPhysicalTime() >= _max_time)
            {
                simulation_0.println(_MSTRING + " Max time reached!");
                simulation_0.saveState(simulation_0.getSessionPath());
                break;
            } 
        }
    }

    private void parse() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(simulation_0.getSessionDir() + File.separator + _settings));

            Pattern p = Pattern.compile("(.+?)=(.+?)?");
            String line;

            while (null != (line = br.readLine())) {
                Matcher m = p.matcher(line);
                if (m.matches()) {
                    switch (m.group(1))
                         {
                             case "TimeStep":
                                 _time_step = Double.parseDouble(m.group(2));
                                 break;
                             case "MaxTime":
                                 _max_time = Double.parseDouble(m.group(2));
                                 break;
                             case "EvalStartTime":
                                 _eval_time = Double.parseDouble(m.group(2));
                                 break;
                             case "UpdateFreq":
                                 _update_freq = Integer.parseInt(m.group(2));
                                 break;
                             case "InnerIter":
                                 _inner_iterations= Integer.parseInt(m.group(2));
                                 break;
                             case "MaxSteps":
                                 _max_steps= Integer.parseInt(m.group(2));
                                 break;
                            case "Eval":
                                _macro = m.group(2);
                                 break; 
                            case "highAcc":
                                 _order = Boolean.parseBoolean(m.group(2));
                                 break;
                            case "meanCFL":
                                 _meanCFL = Double.parseDouble(m.group(2));
                                 break;
                            case "maxCFL":
                                 _maxCFL = Double.parseDouble(m.group(2));
                                 break;
                            case "useCFLControl":
                                 _useCFL = Boolean.parseBoolean(m.group(2));
                                 break;
                            case "pseudoCFL":
                                 _pseudoCFL = Double.parseDouble(m.group(2));
                                 break;
                         }
                }
            }
            br.close();
        } catch (FileNotFoundException ex3) {
            simulation_0.println("Datei mit Einstellungen nicht gefunden!");
        } catch (IOException ex4) {
            simulation_0.println("Datei mit Einstellungen konnte nicht geschlossen werden!");
        }
    }
}
