// Dieses Script nutzt die Datei SetTransient.txt.
// Diese muss IM SELBEM ORDNER WIE DIE SIMULATION liegen
package macro;

import java.util.*;
import java.util.regex.*;
import java.util.Arrays;
import java.io.*;

import star.turbulence.*;
import star.kwturb.*;
import star.keturb.*;
import star.saturb.*;
import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.base.report.*;
import star.post.*;
import star.vis.*;
import star.meshing.*;

import star.flow.PressureCoefficientFunction;
import star.flow.TotalPressureCoefficientFunction;
import star.flow.VelocityMagnitudeProfile;

public class SetTransient extends StarMacro {

    private
        Simulation simulation_0;
        PhysicsContinuum physicsContinuum_0;
        SolutionHistory solutionHistory_0;

        Boolean _history = false;
        double _time_step = 0.01;
        int _max_steps = 1000;
        int _update_freq = 10;
        double _max_time = 1;
        int _inner_iterations = 10;
        String _macro = "Eval.java";
        String _settings = "Transient.settings";
  
    public void execute() {
        prep();  
        simulation_0.println("Settings: " + _settings);
        parse();
        new StarScript(getActiveRootObject(), new File(resolvePath("SetRotation.java"))).play();
        set();
    }

    private void prep() {

        simulation_0 = getActiveSimulation();
        physicsContinuum_0 = ((PhysicsContinuum) simulation_0.getContinuumManager().getContinuum("Physics 1"));


        // CHECK IF UNSTEADY MODELS ALREADY SELECTED
        try {
            physicsContinuum_0.getModelManager().getModel(ImplicitUnsteadyModel.class);
            simulation_0.println("ImplicitUnsteadyModel schon aktiviert!");
        } catch (Exception ex) {
            
            // DISABLE STEADY MODEL
            SteadyModel steadyModel_1 = physicsContinuum_0.getModelManager().getModel(SteadyModel.class);
            physicsContinuum_0.disableModel(steadyModel_1);

            // ENABLE UNSTEADY MODEL
            physicsContinuum_0.enable(ImplicitUnsteadyModel.class);
        }
    }
	
    private void set () {
	  
        // CREATE SOLUTION HISTORY
        if (_history == true) {
                simulation_0.println("Setting History!");
            try {
                solutionHistory_0 = 
                    ((SolutionHistory) simulation_0.get(SolutionHistoryManager.class).getObject(getSimulation().getPresentationName()));
            } catch (Exception ex2) {
                solutionHistory_0 = 
                    simulation_0.get(SolutionHistoryManager.class).createForFile(resolvePath(simulation_0.getSessionDir()+File.separator+simulation_0.getPresentationName()+".simh"), false);
            }
            set_solution_history();
        }

        if (_history == true) {	
            IterationUpdateFrequency iterationUpdateFrequency_0 =
                solutionHistory_0.getUpdate().getIterationUpdateFrequency();
            iterationUpdateFrequency_0.setIterations(_update_freq);
            
            simulation_0.println("Created Solution History!");
        }

        // GET SETTINGS
        ImplicitUnsteadySolver implicitUnsteadySolver_0 =
            ((ImplicitUnsteadySolver) simulation_0.getSolverManager().getSolver(ImplicitUnsteadySolver.class));
        InnerIterationStoppingCriterion innerIterationStoppingCriterion_0 =
            ((InnerIterationStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Maximum Inner Iterations"));
        PhysicalTimeStoppingCriterion physicalTimeStoppingCriterion_0 = 
          ((PhysicalTimeStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Maximum Physical Time"));
        StepStoppingCriterion stepStoppingCriterion_0 = 
          ((StepStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Maximum Steps"));
        AbortFileStoppingCriterion abortFileStoppingCriterion_0 = 
          ((AbortFileStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Stop File"));


        implicitUnsteadySolver_0.getTimeStep().setValue(_time_step);
        innerIterationStoppingCriterion_0.setMaximumNumberInnerIterations(_inner_iterations);
        physicalTimeStoppingCriterion_0.getMaximumTime().setValue(_max_time);
        stepStoppingCriterion_0.setMaximumNumberSteps(_max_steps);
        abortFileStoppingCriterion_0.setIsUsed(true);

        simulation_0.println("Wrote Settings!");

        // GET IMPORT TABLE
        String simulation_name = simulation_0.getPresentationName();
        simulation_name = simulation_name.endsWith("_t") ? simulation_name.substring(0, simulation_name.length()-2) : simulation_name;
        simulation_name = simulation_name.endsWith("_s") ? simulation_name.substring(0, simulation_name.length()-2) : simulation_name;
        FileTable import_table = 
          (FileTable) simulation_0.getTableManager().createFromFile(resolvePath(simulation_0.getSessionDir()+File.separator+simulation_name+".csv"));

        simulation_0.println("Imported Table!");

        // SET PRESSURE
        InitialPressureProfile initialPressureProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(InitialPressureProfile.class);
        initialPressureProfile_0.setMethod(XyzTabularScalarProfileMethod.class);
        initialPressureProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
        initialPressureProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Pressure");

        // SPALART ALLMARAS
        try {
            SpalartAllmarasTurbulence spalartAllmarasTurbulence_0 = 
                physicsContinuum_0.getModelManager().getModel(SpalartAllmarasTurbulence.class);

            physicsContinuum_0.getInitialConditions().get(SaTurbSpecOption.class).setSelected(SaTurbSpecOption.Type.MODIFIED_TURBULENT_DIFFUSIVITY);

            SaTurbDiffusivityProfile saTurbDiffusivityProfile_0 = 
              physicsContinuum_0.getInitialConditions().get(SaTurbDiffusivityProfile.class);
            saTurbDiffusivityProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
            saTurbDiffusivityProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Modified Diffusivity");

        saTurbDiffusivityProfile_0.setMethod(XyzTabularScalarProfileMethod.class);

        } catch (Exception ex6) {}

        // KW
        try {
            KwAllYplusWallTreatment kwAllYplusWallTreatment_0 = 
                physicsContinuum_0.getModelManager().getModel(KwAllYplusWallTreatment.class);

            physicsContinuum_0.getInitialConditions().get(KwTurbSpecOption.class).setSelected(KwTurbSpecOption.Type.K_OMEGA);

            SpecificDissipationRateProfile specificDissipationRateProfile_0 = 
              physicsContinuum_0.getInitialConditions().get(SpecificDissipationRateProfile.class);
            specificDissipationRateProfile_0.setMethod(XyzTabularScalarProfileMethod.class);
            specificDissipationRateProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
            specificDissipationRateProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Specific Dissipation Rate");

            TurbulentKineticEnergyProfile turbulentKineticEnergyProfile_0 = 
              physicsContinuum_0.getInitialConditions().get(TurbulentKineticEnergyProfile.class);
            turbulentKineticEnergyProfile_0.setMethod(XyzTabularScalarProfileMethod.class);
            turbulentKineticEnergyProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
            turbulentKineticEnergyProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Turbulent Kinetic Energy");
        } catch (Exception ex7) {}

        // KE
        try {
            KEpsilonTurbulence kEpsilonTurbulence_1 = 
                physicsContinuum_0.getModelManager().getModel(KEpsilonTurbulence.class);

            physicsContinuum_0.getInitialConditions().get(KeTurbSpecOption.class).setSelected(KeTurbSpecOption.Type.K_EPSILON);

            TurbulentDissipationRateProfile turbulentDissipationRateProfile_0 = 
              physicsContinuum_0.getInitialConditions().get(TurbulentDissipationRateProfile.class);
            turbulentDissipationRateProfile_0.setMethod(XyzTabularScalarProfileMethod.class);
            turbulentDissipationRateProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
            turbulentDissipationRateProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Turbulent Dissipation Rate");

            TurbulentKineticEnergyProfile turbulentKineticEnergyProfile_0 = 
              physicsContinuum_0.getInitialConditions().get(TurbulentKineticEnergyProfile.class);
            turbulentKineticEnergyProfile_0.setMethod(XyzTabularScalarProfileMethod.class);
            turbulentKineticEnergyProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setTable(import_table);
            turbulentKineticEnergyProfile_0.getMethod(XyzTabularScalarProfileMethod.class).setData("Turbulent Kinetic Energy");

        } catch (Exception ex8) {}

        // VELOCITY
        VelocityProfile velocityProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(VelocityProfile.class);
        velocityProfile_0.setMethod(XyzTabularVectorProfileMethod.class);
        velocityProfile_0.getMethod(XyzTabularVectorProfileMethod.class).setTable(import_table);
        velocityProfile_0.getMethod(XyzTabularVectorProfileMethod.class).setXData("Velocity[i]");
        velocityProfile_0.getMethod(XyzTabularVectorProfileMethod.class).setYData("Velocity[j]");
        velocityProfile_0.getMethod(XyzTabularVectorProfileMethod.class).setZData("Velocity[k]");

        // CLEAR SOLUTION
        Solution solution_0 = 
          simulation_0.getSolution();
        solution_0.clearSolution(Solution.Clear.History, Solution.Clear.Fields);

        // INITIALISE SOLUTION
        solution_0.initializeSolution();

        // SAVE
        simulation_name = simulation_name + "_t";
        simulation_0.saveState(simulation_0.getSessionDir()+File.separator+simulation_name+".sim");
    }

    private void parse() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(simulation_0.getSessionDir()+File.separator+_settings));

            Pattern p = Pattern.compile("(.+?)=(.+?)?");
            String line;

            while (null != (line = br.readLine())) {
                Matcher m = p.matcher(line);
                if (m.matches()) {
                    switch (m.group(1))
                         {
                             case "InnerIter":
                                _inner_iterations = Integer.parseInt(m.group(2));
                                 break;
                             case "TimeStep":
                                _time_step = Double.parseDouble(m.group(2));
                                 break;
                             case "MaxTime":
                                _max_time = Double.parseDouble(m.group(2));
                                 break;
                             case "MaxSteps":
                                _max_steps = Integer.parseInt(m.group(2));
                                 break;
                             case "UpdateFreq":
                                _update_freq = Integer.parseInt(m.group(2));
                                 break;
                             case "History":
                                _history = Boolean.parseBoolean(m.group(2));
                                 break;
                            case "Eval":
                                _macro = m.group(2);
                                 break; 
                        }
                }
            }
            br.close();
        } catch (FileNotFoundException ex3) {
            simulation_0.println("Datei mit Einstellungen nicht gefunden!");
        } catch (IOException ex4) {
            simulation_0.println("Datei mit Einstellungen konnte nicht geschlossen werden!");
        }
        simulation_0.println("Parsed");
    }

    private void set_solution_history() {
        PrimitiveFieldFunction primitiveFieldFunction_0 = 
          ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("Pressure"));

        PrimitiveFieldFunction primitiveFieldFunction_1 = 
          ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("Velocity"));

        VectorMagnitudeFieldFunction vectorMagnitudeFieldFunction_0 = 
          ((VectorMagnitudeFieldFunction) primitiveFieldFunction_1.getMagnitudeFunction());

        solutionHistory_0.setFunctions(new NeoObjectVector(new Object[] {primitiveFieldFunction_0, vectorMagnitudeFieldFunction_0}));

        VectorComponentFieldFunction vectorComponentFieldFunction_0 = 
          ((VectorComponentFieldFunction) primitiveFieldFunction_1.getComponentFunction(0));

        VectorComponentFieldFunction vectorComponentFieldFunction_1 = 
          ((VectorComponentFieldFunction) primitiveFieldFunction_1.getComponentFunction(1));

        VectorComponentFieldFunction vectorComponentFieldFunction_2 = 
          ((VectorComponentFieldFunction) primitiveFieldFunction_1.getComponentFunction(2));

        PrimitiveFieldFunction primitiveFieldFunction_2;
        PrimitiveFieldFunction primitiveFieldFunction_3;

        try {
            SpalartAllmarasTurbulence spalartAllmarasTurbulence_0 = 
                physicsContinuum_0.getModelManager().getModel(SpalartAllmarasTurbulence.class);

            primitiveFieldFunction_2 = 
                ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("SaTurbDiffusivity"));
        solutionHistory_0.setFunctions(
            new NeoObjectVector(
                new Object[] {
                    primitiveFieldFunction_0, 
                    vectorMagnitudeFieldFunction_0,
                    vectorComponentFieldFunction_0, 
                    vectorComponentFieldFunction_1, 
                    vectorComponentFieldFunction_2, 
                    primitiveFieldFunction_2,
                }
            )
        );
        } catch (Exception ex7) {}
        try {
            KwAllYplusWallTreatment kwAllYplusWallTreatment_0 = 
                physicsContinuum_0.getModelManager().getModel(KwAllYplusWallTreatment.class);

            primitiveFieldFunction_2 = 
                ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("SpecificDissipationRate"));

            primitiveFieldFunction_3 = 
                ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("TurbulentKineticEnergy"));
        solutionHistory_0.setFunctions(
            new NeoObjectVector(
                new Object[] {
                    primitiveFieldFunction_0, 
                    vectorMagnitudeFieldFunction_0,
                    vectorComponentFieldFunction_0, 
                    vectorComponentFieldFunction_1, 
                    vectorComponentFieldFunction_2, 
                    primitiveFieldFunction_2,
                    primitiveFieldFunction_3
                }
            )
        );
        } catch (Exception ex7) {}
        try {
            KEpsilonTurbulence kEpsilonTurbulence_1 = 
                physicsContinuum_0.getModelManager().getModel(KEpsilonTurbulence.class);

            primitiveFieldFunction_2 = 
                ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("TurbulentDissipationRate"));

            primitiveFieldFunction_3 = 
                ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("TurbulentKineticEnergy"));
        solutionHistory_0.setFunctions(
            new NeoObjectVector(
                new Object[] {
                    primitiveFieldFunction_0, 
                    vectorMagnitudeFieldFunction_0,
                    vectorComponentFieldFunction_0, 
                    vectorComponentFieldFunction_1, 
                    vectorComponentFieldFunction_2, 
                    primitiveFieldFunction_2,
                    primitiveFieldFunction_3
                }
            )
        );
        } catch (Exception ex7) {}
    }
}
