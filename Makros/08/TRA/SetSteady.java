// Dieses Script nutzt die Datei _settings.
// Diese muss IM SELBEM ORDNER WIE DIE SIMULATION liegen
package macro;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import star.turbulence.*;
import star.kwturb.*;
import star.keturb.*;
import star.saturb.*;

import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.post.*;
import star.vis.*;
import star.coupledflow.*;
import star.segregatedflow.*;


public class SetSteady extends StarMacro {

  private 
	  Simulation simulation_0;
	  PhysicsContinuum physicsContinuum_0;
      SolutionHistory solutionHistory_0;
      int _max_steps;
      String _settings = "Steady.settings";

  public void execute() {
	simulation_0 = getActiveSimulation();
	physicsContinuum_0 = ((PhysicsContinuum) simulation_0.getContinuumManager().getContinuum("Physics 1"));

	// CHECK IF UNSTEADY MODELS ALREADY SELECTED
	try {
		physicsContinuum_0.getModelManager().getModel(SteadyModel.class);
		simulation_0.println("SteadyModel schon aktiviert!");
	} catch (Exception ex) {
		
		// DISABLE UNSTEADY MODEL
		ImplicitUnsteadyModel unsteadyModel_1 = physicsContinuum_0.getModelManager().getModel(ImplicitUnsteadyModel.class);
		physicsContinuum_0.disableModel(unsteadyModel_1);

		// ENABLE STEADY MODEL
		physicsContinuum_0.enable(SteadyModel.class);
	}
	
	// DELETE SOLUTION HISTORY
	try {
		solutionHistory_0 = 
			((SolutionHistory) simulation_0.get(SolutionHistoryManager.class).getObject(getSimulation().getPresentationName()));
        simulation_0.get(SolutionHistoryManager.class).removeSolutionHistories(new NeoObjectVector(new Object[] {solutionHistory_0}));
	} catch (Exception ex2) {}

    // CLEAR SOLUTION
    Solution solution_0 = 
      simulation_0.getSolution();
    solution_0.clearSolution(Solution.Clear.History, Solution.Clear.Fields);
	
	// GET SETTINGS
	StepStoppingCriterion stepStoppingCriterion_0 = 
	  ((StepStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Maximum Steps"));
    InitialPressureProfile initialPressureProfile_0 = 
      physicsContinuum_0.getInitialConditions().get(InitialPressureProfile.class);
    VelocityProfile velocityProfile_0 = 
        physicsContinuum_0.getInitialConditions().get(VelocityProfile.class);
    AutoSave autoSave_0 = 
      simulation_0.getSimulationIterator().getAutoSave();
    StarUpdate starUpdate_0 = 
      autoSave_0.getStarUpdate();
    IterationUpdateFrequency iterationUpdateFrequency_0 = 
      starUpdate_0.getIterationUpdateFrequency();
    AbortFileStoppingCriterion abortFileStoppingCriterion_0 = 
      ((AbortFileStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Stop File"));
	
	// SET SETTINGS
	parse();
	stepStoppingCriterion_0.setMaximumNumberSteps(_max_steps);
    initialPressureProfile_0.setMethod(ConstantScalarProfileMethod.class);


    // TURB
    // SPALART ALLMARAS
    try {
        SpalartAllmarasTurbulence spalartAllmarasTurbulence_0 = 
            physicsContinuum_0.getModelManager().getModel(SpalartAllmarasTurbulence.class);

        physicsContinuum_0.getInitialConditions().get(SaTurbSpecOption.class).setSelected(SaTurbSpecOption.Type.TURBULENT_VISCOSITY_RATIO);
        
        TurbulentViscosityRatioProfile turbulentViscosityRatioProfile_0 = 
            physicsContinuum_0.getInitialConditions().get(TurbulentViscosityRatioProfile.class);

        turbulentViscosityRatioProfile_0.setMethod(ConstantScalarProfileMethod.class);
    } catch (Exception ex6) {}

    // KW
    try {
        KwAllYplusWallTreatment kwAllYplusWallTreatment_0 = 
            physicsContinuum_0.getModelManager().getModel(KwAllYplusWallTreatment.class);

        // KOmegaTurbulence kOmegaTurbulence_0 = 
            // physicsContinuum_0.getModelManager().getModel(KOmegaTurbulence.class);

        physicsContinuum_0.getInitialConditions().get(KwTurbSpecOption.class).setSelected(KwTurbSpecOption.Type.INTENSITY_VISCOSITY_RATIO);
         
        TurbulentVelocityScaleProfile turbulentVelocityScaleProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(TurbulentVelocityScaleProfile.class);
        turbulentVelocityScaleProfile_0.setMethod(ConstantScalarProfileMethod.class);

        TurbulentViscosityRatioProfile turbulentViscosityRatioProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(TurbulentViscosityRatioProfile.class);
        turbulentViscosityRatioProfile_0.setMethod(ConstantScalarProfileMethod.class);

        TurbulenceIntensityProfile turbulenceIntensityProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(TurbulenceIntensityProfile.class);
        turbulenceIntensityProfile_0.setMethod(ConstantScalarProfileMethod.class);

    } catch (Exception ex7) {}

    // KE
    try {
        KEpsilonTurbulence kEpsilonTurbulence_1 = 
            physicsContinuum_0.getModelManager().getModel(KEpsilonTurbulence.class);

        physicsContinuum_0.getInitialConditions().get(KeTurbSpecOption.class).setSelected(KeTurbSpecOption.Type.INTENSITY_VISCOSITY_RATIO);

        TurbulentVelocityScaleProfile turbulentVelocityScaleProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(TurbulentVelocityScaleProfile.class);
        turbulentVelocityScaleProfile_0.setMethod(ConstantScalarProfileMethod.class);

        TurbulentViscosityRatioProfile turbulentViscosityRatioProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(TurbulentViscosityRatioProfile.class);
        turbulentViscosityRatioProfile_0.setMethod(ConstantScalarProfileMethod.class);

        TurbulenceIntensityProfile turbulenceIntensityProfile_0 = 
          physicsContinuum_0.getInitialConditions().get(TurbulenceIntensityProfile.class);
        turbulenceIntensityProfile_0.setMethod(ConstantScalarProfileMethod.class);
    } catch (Exception ex8) {}

    // VELOCITY

    velocityProfile_0.setMethod(ConstantVectorProfileMethod.class);
    velocityProfile_0.getMethod(ConstantVectorProfileMethod.class).getQuantity().setComponents(16.11, 0.0, 0.0);
    abortFileStoppingCriterion_0.setIsUsed(true);
    iterationUpdateFrequency_0.setIterations(200);

	// SAVE
    String simulation_name = simulation_0.getPresentationName();
    simulation_name = simulation_name.endsWith("_t") ? simulation_name.substring(0, simulation_name.length()-2) : simulation_name;
    simulation_name = simulation_name.endsWith("_s") ? simulation_name : simulation_name + "_s";

	simulation_0.saveState(simulation_0.getSessionDir()+File.separator+simulation_name+".sim");
  }

  private void parse() {

	try {
		BufferedReader br = new BufferedReader(new FileReader(simulation_0.getSessionDir()+File.separator+_settings));

		Pattern p = Pattern.compile("(.+?)=(.+?)?");
		String line;

		while (null != (line = br.readLine())) {
			Matcher m = p.matcher(line);
			if (m.matches()) {
				switch (m.group(1))
					 {
						 case "MaxSteps":
							 _max_steps = Integer.parseInt(m.group(2));
							 break;
					 }
			}
		}
		br.close();
	} catch (FileNotFoundException ex3) {
		simulation_0.println("Datei mit Einstellungen nicht gefunden!");
	} catch (IOException ex4) {
		simulation_0.println("Datei mit Einstellungen konnte nicht geschlossen werden!");
	}
  }
}
