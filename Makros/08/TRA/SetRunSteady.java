package macro;

import java.util.*;
import java.io.*;

import star.turbulence.*;
import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.base.report.*;
import star.post.*;
import star.vis.*;
import star.meshing.*;

public class SetRunTransient extends StarMacro {

    public void execute() {
        new StarScript(getActiveRootObject(), new File(resolvePath("SetSteady.java"))).play();
        new StarScript(getActiveRootObject(), new File(resolvePath("RunSteady.java"))).play();
    }
}
