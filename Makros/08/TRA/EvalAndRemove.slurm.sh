#!/usr/bin/env zsh
#SBATCH -J ev_and_remove
#SBATCH --mail-user=erik.schneidereit@rwth-aachen.de
#SBATCH --mail-type=ALL

#SBATCH -t 02:00:00
#SBATCH  --mem-per-cpu=30000M
#SBATCH  --ntasks-per-node=4
#SBATCH  --cpus-per-task=1
#SBATCH  --ntasks-per-core=1
#SBATCH  --nodes=1
#SBATCH  -o Eval.out
module load TECHNICS
module load starccm/13.02
module switch openmpi intelmpi
export I_MPI_FABRICS="shm:ofi"
export I_MPI_OFI_PROVIDER=psm2
cd $SLURM_SUBMIT_DIR
scontrol show hostname $SLURM_NODELIST | tr 'ec' 'ic'> machinefile_${SLURM_JOB_ID}
