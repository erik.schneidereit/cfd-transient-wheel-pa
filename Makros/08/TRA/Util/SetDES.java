// STAR-CCM+ macro: SetDES.java
// Written by STAR-CCM+ 13.02.013
package macro;

import java.util.*;

import star.common.*;
import star.material.*;
import star.coupledflow.*;
import star.turbulence.*;
import star.kwturb.*;
import star.flow.*;
import star.walldistance.*;
import star.metrics.*;

public class SetDES extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    PhysicsContinuum physicsContinuum_0 = 
      ((PhysicsContinuum) simulation_0.getContinuumManager().getContinuum("Physics 1"));

    ImplicitUnsteadyModel implicitUnsteadyModel_0 = 
      physicsContinuum_0.getModelManager().getModel(ImplicitUnsteadyModel.class);

    physicsContinuum_0.disableModel(implicitUnsteadyModel_0);

    KwAllYplusWallTreatment kwAllYplusWallTreatment_0 = 
      physicsContinuum_0.getModelManager().getModel(KwAllYplusWallTreatment.class);

    physicsContinuum_0.disableModel(kwAllYplusWallTreatment_0);

    WallDistanceModel wallDistanceModel_0 = 
      physicsContinuum_0.getModelManager().getModel(WallDistanceModel.class);

    physicsContinuum_0.disableModel(wallDistanceModel_0);

    SstKwTurbModel sstKwTurbModel_0 = 
      physicsContinuum_0.getModelManager().getModel(SstKwTurbModel.class);

    physicsContinuum_0.disableModel(sstKwTurbModel_0);

    KOmegaTurbulence kOmegaTurbulence_0 = 
      physicsContinuum_0.getModelManager().getModel(KOmegaTurbulence.class);

    physicsContinuum_0.disableModel(kOmegaTurbulence_0);

    RansTurbulenceModel ransTurbulenceModel_0 = 
      physicsContinuum_0.getModelManager().getModel(RansTurbulenceModel.class);

    physicsContinuum_0.disableModel(ransTurbulenceModel_0);

    ConstantDensityModel constantDensityModel_0 = 
      physicsContinuum_0.getModelManager().getModel(ConstantDensityModel.class);

    physicsContinuum_0.disableModel(constantDensityModel_0);

    TurbulentModel turbulentModel_0 = 
      physicsContinuum_0.getModelManager().getModel(TurbulentModel.class);

    physicsContinuum_0.disableModel(turbulentModel_0);

    GradientsModel gradientsModel_0 = 
      physicsContinuum_0.getModelManager().getModel(GradientsModel.class);

    physicsContinuum_0.disableModel(gradientsModel_0);

    CoupledFlowModel coupledFlowModel_0 = 
      physicsContinuum_0.getModelManager().getModel(CoupledFlowModel.class);

    physicsContinuum_0.disableModel(coupledFlowModel_0);

    ThreeDimensionalModel threeDimensionalModel_0 = 
      physicsContinuum_0.getModelManager().getModel(ThreeDimensionalModel.class);

    physicsContinuum_0.disableModel(threeDimensionalModel_0);

    SingleComponentGasModel singleComponentGasModel_0 = 
      physicsContinuum_0.getModelManager().getModel(SingleComponentGasModel.class);

    physicsContinuum_0.disableModel(singleComponentGasModel_0);

    physicsContinuum_0.enable(ThreeDimensionalModel.class);

    physicsContinuum_0.enable(ImplicitUnsteadyModel.class);

    physicsContinuum_0.enable(SingleComponentGasModel.class);

    physicsContinuum_0.enable(CoupledFlowModel.class);

    physicsContinuum_0.enable(ConstantDensityModel.class);

    physicsContinuum_0.enable(TurbulentModel.class);

    physicsContinuum_0.enable(DesTurbulenceModel.class);

    physicsContinuum_0.enable(SstKwTurbDesModel.class);

    physicsContinuum_0.enable(KwAllYplusWallTreatment.class);
  }
}
