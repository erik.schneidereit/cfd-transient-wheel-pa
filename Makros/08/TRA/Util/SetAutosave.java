// STAR-CCM+ macro: SetAutosave.java
// Written by STAR-CCM+ 12.02.011
package macro;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import star.turbulence.*;
import star.keturb.*;
import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.post.*;
import star.vis.*;

public class SetAutosave extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    AutoSave autoSave_0 = 
      simulation_0.getSimulationIterator().getAutoSave();

    StarUpdate starUpdate_0 = 
      autoSave_0.getStarUpdate();

    IterationUpdateFrequency iterationUpdateFrequency_0 = 
      starUpdate_0.getIterationUpdateFrequency();

    iterationUpdateFrequency_0.setIterations(200);

    AbortFileStoppingCriterion abortFileStoppingCriterion_0 = 
      ((AbortFileStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Stop File"));
    abortFileStoppingCriterion_0.setIsUsed(true);

	simulation_0.saveState(simulation_0.getSessionDir()+File.separator+simulation_0.getPresentationName()+".sim");
  }
}
