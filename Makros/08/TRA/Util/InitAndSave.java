// STAR-CCM+ macro: ClearSolution.java
// Written by STAR-CCM+ 12.02.011
package macro;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import star.turbulence.*;
import star.keturb.*;
import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.post.*;
import star.meshing.*;

public class InitAndSave extends StarMacro {

  public void execute() {

    Simulation simulation_0 = 
      getActiveSimulation();

    Solution solution_0 = 
      simulation_0.getSolution();

    solution_0.initializeSolution();

    simulation_0.saveState(simulation_0.getSessionDir()+File.separator+simulation_0.getPresentationName()+".sim");
  }
}
