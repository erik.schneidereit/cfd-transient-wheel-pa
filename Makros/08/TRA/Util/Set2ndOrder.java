// STAR-CCM+ macro: Set2ndOrder.java
// Written by STAR-CCM+ 13.02.013
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;

public class Set2ndOrder extends StarMacro {

  public void execute() {
    Simulation simulation_0 = 
      getActiveSimulation();

    ImplicitUnsteadySolver implicitUnsteadySolver_0 = 
      ((ImplicitUnsteadySolver) simulation_0.getSolverManager().getSolver(ImplicitUnsteadySolver.class));

    implicitUnsteadySolver_0.getTimeDiscretizationOption().setSelected(TimeDiscretizationOption.Type.SECOND_ORDER);
    // implicitUnsteadySolver_0.getTimeStep().setValue(0.01);
  }
}
