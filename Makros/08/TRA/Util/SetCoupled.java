// STAR-CCM+ macro: SetCoupled.java
// Written by STAR-CCM+ 12.02.011
package macro;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import star.turbulence.*;
import star.keturb.*;
import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.post.*;
import star.vis.*;
import star.coupledflow.*;
import star.segregatedflow.*;

public class SetCoupled extends StarMacro {

public void execute() {

	Simulation simulation_0 = getActiveSimulation();
	Solution solution_0 = simulation_0.getSolution();

	PhysicsContinuum physicsContinuum_0 = 
	  ((PhysicsContinuum) simulation_0.getContinuumManager().getContinuum("Physics 1"));

	SegregatedFlowModel segregatedFlowModel_0 = 
	  physicsContinuum_0.getModelManager().getModel(SegregatedFlowModel.class);

	physicsContinuum_0.disableModel(segregatedFlowModel_0);
	physicsContinuum_0.enable(CoupledFlowModel.class);

    solution_0.clearSolution(Solution.Clear.History, Solution.Clear.Fields);
	simulation_0.saveState(simulation_0.getSessionDir()+File.separator+simulation_0.getPresentationName()+".sim");
}
}
