// STAR-CCM+ macro: SetRotation.java
// Written by STAR-CCM+ 13.02.013
package macro;

import java.util.*;

import star.common.*;
import star.motion.*;

public class SetRotation extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    Region region_0 = 
      simulation_0.getRegionManager().getRegion("Rim");

    MotionSpecification motionSpecification_0 = 
      region_0.getValues().get(MotionSpecification.class);

    RotatingMotion rotatingMotion_0 = 
      ((RotatingMotion) simulation_0.get(MotionManager.class).getObject("Rotation"));

    motionSpecification_0.setMotion(rotatingMotion_0);
	simulation_0.saveState(getSimulation().getPresentationName()+".sim");
  }
}
