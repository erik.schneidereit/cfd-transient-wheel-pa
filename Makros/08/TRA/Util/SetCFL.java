// STAR-CCM+ macro: SetCFL.java
// Written by STAR-CCM+ 13.02.013
package macro;
import java.util.*;
import java.util.regex.*;
import java.io.*;

import star.turbulence.*;
import star.keturb.*;
import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.post.*;
import star.vis.*;
import star.coupledflow.*;

public class SetCFL extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    CoupledImplicitSolver coupledImplicitSolver_0 = 
      ((CoupledImplicitSolver) simulation_0.getSolverManager().getSolver(CoupledImplicitSolver.class));

    coupledImplicitSolver_0.setCFL(10.0);

    coupledImplicitSolver_0.getSolutionDriverManager().getExpertDriverOption().setSelected(ExpertDriverOption.Type.EXPERT_DRIVER);
	simulation_0.saveState(simulation_0.getSessionDir()+File.separator+simulation_0.getPresentationName()+".sim");
  }
}
