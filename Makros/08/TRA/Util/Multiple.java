package macro;

import java.util.*;
import java.io.*;

import star.turbulence.*;
import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.base.report.*;
import star.post.*;
import star.vis.*;
import star.meshing.*;

public class Multiple extends StarMacro {

    public void execute() {
        // Add multiple starccm macros to be executed here :)
        new StarScript(getActiveRootObject(), new File(resolvePath("ExportFields.java"))).play();
        new StarScript(getActiveRootObject(), new File(resolvePath("SetTransient.java"))).play();
    }
}
