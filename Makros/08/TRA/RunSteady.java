package macro;

import java.util.*;
import java.util.regex.*;
import java.util.Arrays;
import java.io.*;

import star.turbulence.*;
import star.kwturb.*;
import star.keturb.*;
import star.saturb.*;
import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.base.report.*;
import star.post.*;
import star.vis.*;
import star.meshing.*;

public class RunSteady extends StarMacro {

    private String _settings = "Steady.settings";
    private String _macro;
    private Simulation simulation_0;

    public void execute() {
        simulation_0 = getActiveSimulation();
        parse();
        steady_run();
    }
   private void steady_run() {
    StepStoppingCriterion stepStoppingCriterion_0 = 
      ((StepStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Maximum Steps"));
        stepStoppingCriterion_0.setMaximumNumberSteps(1000);

        String simulation_name = simulation_0.getPresentationName();

        simulation_name = simulation_name.endsWith("_t") ? simulation_name.substring(0, simulation_name.length()-2) : simulation_name;
        simulation_name = simulation_name.endsWith("_s") ? simulation_name.substring(0, simulation_name.length()-2) : simulation_name;
	    simulation_0.saveState(simulation_0.getSessionDir()+File.separator + simulation_name + ".sim");
        simulation_0.getSimulationIterator().run();
        new StarScript(getActiveRootObject(), new File(resolvePath(_macro))).play();
    }

    private void parse() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(simulation_0.getSessionDir() + File.separator + _settings));

            Pattern p = Pattern.compile("(.+?)=(.+?)?");
            String line;

            while (null != (line = br.readLine())) {
                Matcher m = p.matcher(line);
                if (m.matches()) {
                    switch (m.group(1))
                         {
                            case "Eval":
                                _macro = m.group(2);
                                 break; 
                         }
                }
            }
            br.close();
        } catch (FileNotFoundException ex3) {
            simulation_0.println("Datei mit Einstellungen nicht gefunden!");
        } catch (IOException ex4) {
            simulation_0.println("Datei mit Einstellungen konnte nicht geschlossen werden!");
        }
    }
}
