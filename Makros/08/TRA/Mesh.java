// STAR-CCM+ macro: Mesh_ADJ.java
// Written by STAR-CCM+ 12.02.011
package macro;

import java.util.*;

import star.common.*;
import star.meshing.*;

public class Mesh extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    MeshPipelineController meshPipelineController_0 = simulation_0.get(MeshPipelineController.class);

	simulation_0.saveState(getSimulation().getPresentationName()+".sim");
    meshPipelineController_0.generateVolumeMesh();
	simulation_0.saveState(getSimulation().getPresentationName()+".sim");
  }
}
