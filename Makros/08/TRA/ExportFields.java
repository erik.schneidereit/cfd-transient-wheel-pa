// STAR-CCM+ macro: ExportFields.java
// Written by STAR-CCM+ 12.02.011
package macro;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import star.turbulence.*;
import star.kwturb.*;
import star.keturb.*;
import star.saturb.*;

import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.post.*;
import star.vis.*;
import star.coupledflow.*;
import star.segregatedflow.*;

public class ExportFields extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();
	PhysicsContinuum physicsContinuum_0 = ((PhysicsContinuum) simulation_0.getContinuumManager().getContinuum("Physics 1"));

    XyzInternalTable xyzInternalTable_0 = 
      simulation_0.getTableManager().createTable(XyzInternalTable.class);

    PrimitiveFieldFunction primitiveFieldFunction_0 = 
      ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("Pressure"));

    PrimitiveFieldFunction primitiveFieldFunction_1 = 
      ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("Velocity"));

    VectorMagnitudeFieldFunction vectorMagnitudeFieldFunction_0 = 
      ((VectorMagnitudeFieldFunction) primitiveFieldFunction_1.getMagnitudeFunction());

    VectorComponentFieldFunction vectorComponentFieldFunction_0 = 
      ((VectorComponentFieldFunction) primitiveFieldFunction_1.getComponentFunction(0));

    VectorComponentFieldFunction vectorComponentFieldFunction_1 = 
      ((VectorComponentFieldFunction) primitiveFieldFunction_1.getComponentFunction(1));

    VectorComponentFieldFunction vectorComponentFieldFunction_2 = 
      ((VectorComponentFieldFunction) primitiveFieldFunction_1.getComponentFunction(2));

    PrimitiveFieldFunction primitiveFieldFunction_2;
    PrimitiveFieldFunction primitiveFieldFunction_3;

    try {
        SpalartAllmarasTurbulence spalartAllmarasTurbulence_0 = 
            physicsContinuum_0.getModelManager().getModel(SpalartAllmarasTurbulence.class);

        primitiveFieldFunction_2 = 
            ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("SaTurbDiffusivity"));
        xyzInternalTable_0.setFieldFunctions(new NeoObjectVector(new Object[] {primitiveFieldFunction_0, vectorMagnitudeFieldFunction_0, vectorComponentFieldFunction_0, vectorComponentFieldFunction_1, vectorComponentFieldFunction_2, primitiveFieldFunction_2}));
    } catch (Exception ex7) {}
    try {
        KwAllYplusWallTreatment kwAllYplusWallTreatment_0 = 
            physicsContinuum_0.getModelManager().getModel(KwAllYplusWallTreatment.class);

        primitiveFieldFunction_2 = 
            ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("SpecificDissipationRate"));

        primitiveFieldFunction_3 = 
            ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("TurbulentKineticEnergy"));
        xyzInternalTable_0.setFieldFunctions(new NeoObjectVector(new Object[] {primitiveFieldFunction_0, vectorMagnitudeFieldFunction_0, vectorComponentFieldFunction_0, vectorComponentFieldFunction_1, vectorComponentFieldFunction_2, primitiveFieldFunction_2, primitiveFieldFunction_3}));
    } catch (Exception ex7) {}
    try {
        KEpsilonTurbulence kEpsilonTurbulence_1 = 
            physicsContinuum_0.getModelManager().getModel(KEpsilonTurbulence.class);

        primitiveFieldFunction_2 = 
            ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("TurbulentDissipationRate"));

        primitiveFieldFunction_3 = 
            ((PrimitiveFieldFunction) simulation_0.getFieldFunctionManager().getFunction("TurbulentKineticEnergy"));
        xyzInternalTable_0.setFieldFunctions(new NeoObjectVector(new Object[] {primitiveFieldFunction_0, vectorMagnitudeFieldFunction_0, vectorComponentFieldFunction_0, vectorComponentFieldFunction_1, vectorComponentFieldFunction_2, primitiveFieldFunction_2, primitiveFieldFunction_3}));
    } catch (Exception ex7) {}


    xyzInternalTable_0.getParts().setQuery(null);

    Region region_0 = 
      simulation_0.getRegionManager().getRegion("Fluid");

    xyzInternalTable_0.getParts().setObjects(region_0);

    xyzInternalTable_0.extract();

    String simulation_name = simulation_0.getPresentationName();
    simulation_name = simulation_name.endsWith("_t") ? simulation_name.substring(0, simulation_name.length()-2) : simulation_name;
    simulation_name = simulation_name.endsWith("_s") ? simulation_name.substring(0, simulation_name.length()-2) : simulation_name;
    xyzInternalTable_0.export(resolvePath(simulation_0.getSessionDir()+File.separator+simulation_name+".csv"));

    simulation_0.getTableManager().remove(xyzInternalTable_0);
  }
}
