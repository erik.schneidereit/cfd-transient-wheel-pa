package macro;

import java.util.*;
import java.util.regex.*;
import java.util.Arrays;
import java.io.*;
import java.text.DecimalFormat;

import star.turbulence.*;
import star.kwturb.*;
import star.keturb.*;
import star.saturb.*;
import star.flow.*;
import star.common.*;
import star.base.neo.*;
import star.base.report.*;
import star.post.*;
import star.vis.*;
import star.meshing.*;

import star.flow.PressureCoefficientFunction;
import star.flow.TotalPressureCoefficientFunction;
import star.flow.VelocityMagnitudeProfile;

public class EvalParse extends StarMacro {

    private int currentIterations;
    private boolean keepVis = false;
    private double inletSpeed;
    private Simulation activeSim;
    private PhysicsContinuum continuum;
    List<PartSurface> carparts;
    HashSet regions;
    private double physicalTime;
    DecimalFormat timeFormat = new DecimalFormat("#.00000");
    String _sim_settings = "Sim.settings";
    String _view_settings = "View.settings";
    String _eval_settings = "Eval.settings";

    List<DoubleVector> focalpoints  = new ArrayList<DoubleVector>();
    List<DoubleVector> pointsOfView = new ArrayList<DoubleVector>();
    List<DoubleVector> viewsUp      = new ArrayList<DoubleVector>();
    List<Double> viewsZoom          = new ArrayList<Double>();
    List<String> viewsName          = new ArrayList<String>();
    int numViews = 0;
    
    public void execute() {
        activeSim = getActiveSimulation();
        File noeval = new File("NOEVAL");
        File onceeval= new File("ONCEEVAL");
        File smalleval= new File("SMALLEVAL");
        File mesheval= new File("MESHEVAL");
        if(mesheval.exists())
        {
            _eval_settings = "EvalMesh.settings";
            activeSim.println("Performing mesh evaluation only.");
            mesheval.delete();
            eval();
        } 
        if(onceeval.exists())
        {
            _eval_settings = "Eval.settings";
            activeSim.println("Performing one evaluation.");
            onceeval.delete();
            eval();
        } 
        else if(smalleval.exists())
        {
            _eval_settings = "EvalSmall.settings";
            activeSim.println("Performing small evaluation.");
            eval();
        }
        else if(noeval.exists())
        {
            activeSim.println("Skipping Evaluation.");
        } 
        else
        {
            _eval_settings = "Eval.settings";
            eval();
        }
    }

    public void eval() {
    
        //Einlesen aktuelle Simulation
        activeSim = getActiveSimulation();
	    continuum = ((PhysicsContinuum) activeSim.getContinuumManager().getContinuum("Physics 1"));

        //Einlesen der Anzahl der Iterationen
        currentIterations = activeSim.getSimulationIterator().getCurrentIteration();
        physicalTime = activeSim.getSolution().getPhysicalTime();

        //Einlesen der Regions und der darzustellenden Flaechen aus der Datei Sim.settings
        parseSim();
        parseView();

        /*
        carparts = new ArrayList<PartSurface>();
        carparts.addAll(getSurfaces("Car", "Fluid"));
        carparts.addAll(getSurfaces("Wheel Assembly", "Fluid"));

        Region fluid = activeSim.getRegionManager().getRegion("Fluid");
        Region rim = activeSim.getRegionManager().getRegion("Rim");
        // Region RIM_L = activeSim.getRegionManager().getRegion("RIM_L");
        // Region RIM_R = activeSim.getRegionManager().getRegion("RIM_R");
        // Region Front = activeSim.getRegionManager().getRegion("Front");
        // Region Rear = activeSim.getRegionManager().getRegion("Rear");

        regions = new HashSet();
        regions.add(fluid);
        regions.add(rim);
        // regions.add(RIM_L);
        // regions.add(RIM_R);
        // regions.add(Front);
        // regions.add(Rear);
        */
        
        //Settings for pressure coefficients
        double refDensity = 1.18415;

        PressureCoefficientFunction pressureCoefficientFunction_0 = 
            ((PressureCoefficientFunction) activeSim.getFieldFunctionManager().getFunction("PressureCoefficient"));

        pressureCoefficientFunction_0.getReferenceDensity().setValue(refDensity);
        pressureCoefficientFunction_0.getReferenceVelocity().setValue(16.11);

        TotalPressureCoefficientFunction totalPressureCoefficientFunction_0 = 
            ((TotalPressureCoefficientFunction) activeSim.getFieldFunctionManager().getFunction("TotalPressureCoefficient"));

        totalPressureCoefficientFunction_0.getReferenceDensity().setValue(refDensity);
        totalPressureCoefficientFunction_0.getReferenceVelocity().setValue(16.11);
        
        parseEval();
    }

    private void parseView() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(activeSim.getSessionDir() + File.separator + _view_settings));

            Pattern header = Pattern.compile("(\\[)(.+)(\\])");
            Pattern element = Pattern.compile("([\\w]+)=([\\w.,-]+|[\\\"].+[\\\"])([ ][\\w.,-]+|[ ][\\\"].+[\\\"])?([ ][\\w.,-]+|[ ][\\\"].+[\\\"])?");
            String line;

            while (null != (line = br.readLine())) {
                activeSim.println(line);
                if (line.isEmpty()) continue;
                if (line.startsWith("#")) continue;
                Matcher m = header.matcher(line);
                if (m.matches()) {
                    switch (m.group(2)) {
                        case "View": {
                            numViews += 1;
                            while (null != (line = br.readLine()))  {
                                activeSim.println(line);
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;
                                line = line.replace("\"", "");
                                m = element.matcher(line);
                                if (m.matches()) {
                                    String str = m.group(2);
                                    str = str.replace("\"", "");
                                    switch (m.group(1)) {
                                        case "focalpoint":
                                            focalpoints.add(new DoubleVector(new double[] {
                                                Double.parseDouble(m.group(2).replace("\"", "")),
                                                Double.parseDouble(m.group(3).replace("\"", "")),
                                                Double.parseDouble(m.group(4).replace("\"", ""))} ));
                                            break;
                                        case "location":
                                            pointsOfView.add(new DoubleVector(new double[] {
                                                Double.parseDouble(m.group(2).replace("\"", "")),
                                                Double.parseDouble(m.group(3).replace("\"", "")),
                                                Double.parseDouble(m.group(4).replace("\"", ""))} ));
                                            break;
                                        case "viewUp":
                                            viewsUp.add(new DoubleVector(new double[] {
                                                Double.parseDouble(m.group(2).replace("\"", "")),
                                                Double.parseDouble(m.group(3).replace("\"", "")),
                                                Double.parseDouble(m.group(4).replace("\"", ""))} ));
                                            break;
                                        case "zoom":
                                            viewsZoom.add(Double.parseDouble(str));
                                            break;
                                        case "name":
                                            viewsName.add(str);
                                            break;
                                        default: activeSim.println("Input <<" + line + ">> unreadable");
                                    }
                                }
                                else activeSim.println("Input <<" + line + " >> unreadable");
                            }
                        }
                    }
                }
            }
        } catch (Exception SimParseEx) {activeSim.println("Parse of simulation settings failed!");}
    }

    private void parseSim() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(activeSim.getSessionDir() + File.separator + _sim_settings));

            Pattern header = Pattern.compile("(\\[)(.+)(\\])");
            Pattern surf_pattern = Pattern.compile("([\\\"][\\w]+[\\\"])[ ]([\\\"][\\w]+[\\\"])");
            String line;

            while (null != (line = br.readLine())) {
                activeSim.println(line);
                if (line.isEmpty()) continue;
                if (line.startsWith("#")) continue;
                Matcher m = header.matcher(line);
                if (m.matches()) {
                    switch (m.group(2)) {
                        case "Regions": {
                            regions = new HashSet();
                            while (null != (line = br.readLine()))  {
                                activeSim.println(line);
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;
                                line = line.replace("\"", "");
                                Region region = activeSim.getRegionManager().getRegion(line);
                                regions.add(region);
                            }
                        }
                        case "Surfaces": {
                            carparts = new ArrayList<PartSurface>();
                            while (null != (line = br.readLine()))  {
                                activeSim.println(line);
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;

                                m = surf_pattern.matcher(line);
                                if (m.matches()) {
                                    String str1 = m.group(1);
                                    str1 = str1.replace("\"", "");
                                    String str2 = m.group(2);
                                    str2 = str2.replace("\"", "");
                                    carparts.addAll(getSurfaces(str1, str2));
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception SimParseEx) {activeSim.println("Parse of simulation settings failed!");}
    }

    private void parseEval() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(activeSim.getSessionDir() + File.separator + _eval_settings));

            Pattern header = Pattern.compile("(\\[)(.+)(\\])");
            // Pattern element = Pattern.compile("([\\w]+)=([\\w., ]+)");
            Pattern element = Pattern.compile("([\\w]+)=([\\w.,-]+|[\\\"].+[\\\"])([ ][\\w.,-]+|[ ][\\\"].+[\\\"])?([ ][\\w.,-]+|[ ][\\\"].+[\\\"])?");
            String line;

            while (null != (line = br.readLine())) {
                if (line.isEmpty()) continue;
                if (line.startsWith("#")) continue;
                Matcher m = header.matcher(line);
                if (m.matches()) {
                    switch (m.group(2)) {
                        case "PointData": {
                            double step = 0.1, start = 0;
                            int num = 1;
                            String name = "", function = "Velocity";

                            while (null != (line = br.readLine()))  {
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;
                                m = element.matcher(line);
                                if (m.matches()) {
                                    String str = m.group(2);
                                    str = str.replace("\"", "");
                                    switch (m.group(1)) {
                                        case "start":
                                            start = Double.parseDouble(str); 
                                            break;
                                        case "num":
                                            num = Integer.parseInt(str); 
                                            break;
                                        case "step":
                                            step = Double.parseDouble(str); 
                                            break;
                                        case "name":
                                            name = str;
                                            break;
                                        case "function":
                                            function = str;
                                            break;
                                        default: activeSim.println("Input <<" + line + ">> unreadable");
                                    }
                                }
                                else activeSim.println("Input <<" + line + " >> unreadable");
                            }
	                        createPlane(name, function, start, step, num);
                        }
                        break;
                        case "Sections": {
                            double min = 0, max = 100, step = 0.1, start = 0, end = 1, zoom = 1;
                            DoubleVector focalpoint = new DoubleVector(new double[] {0, 0, 0.2} );
                            DoubleVector location = new DoubleVector(new double[] {-1, 0, 0.2} );
                            boolean showMesh = false;
                            String name = "", function = "Velocity", scalar = "Magnitude", path = "Eval";

                            while (null != (line = br.readLine()))  {
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;
                                m = element.matcher(line);
                                if (m.matches()) {
                                    String str = m.group(2);
                                    str = str.replace("\"", "");
                                    switch (m.group(1)) {
                                        case "focalpoint":
                                            focalpoint = new DoubleVector(new double[] {
                                                Double.parseDouble(m.group(2).replace("\"", "")),
                                                Double.parseDouble(m.group(3).replace("\"", "")),
                                                Double.parseDouble(m.group(4).replace("\"", ""))} );
                                            break;
                                        case "location":
                                            location = new DoubleVector(new double[] {
                                                Double.parseDouble(m.group(2).replace("\"", "")),
                                                Double.parseDouble(m.group(3).replace("\"", "")),
                                                Double.parseDouble(m.group(4).replace("\"", ""))} );
                                            break;
                                        case "zoom":
                                            zoom = Double.parseDouble(str);
                                            break;
                                        case "start":
                                            start = Double.parseDouble(str); 
                                            break;
                                        case "end":
                                            
                                            end = Double.parseDouble(str); 
                                            break;
                                        case "step":
                                            
                                            step = Double.parseDouble(str); 
                                            break;
                                        case "name":
                                            
                                            name = str;
                                            break;
                                        case "function":
                                            
                                            function = str;
                                            break;
                                        case "scalar":
                                            
                                            scalar = str;
                                            break;
                                        case "min":
                                            
                                            min = Double.parseDouble(str); 
                                            break;
                                        case "max":
                                            
                                            max = Double.parseDouble(str); 
                                            break;
                                        case "showMesh":
                                            
                                            showMesh = Boolean.parseBoolean(str); 
                                            break;
                                        default: activeSim.println("Input <<" + line + ">> unreadable");
                                    }
                                }
                                else activeSim.println("Input <<" + line + " >> unreadable");
                            }
                            FieldFunction ff = parseFieldFunction(function, scalar);
                            planeExport(function, name, ff, min, max, step, start, end, focalpoint, location, zoom, showMesh);
                        }
                        break;
                        case "IsoPic": {
                            double iso_min = 0, iso_max = 100;
                            double scalar_min = 0, scalar_max = 100;
                            boolean setSym = false;
                            String name = "", iso_function = "Pressure", iso_scalar = "Magnitude", path = "Eval", legendStyle = "spectrum";
                            String scalar_function = "Pressure", scalar_scalar = "Magnitude"; 
                            
                            while (null != (line = br.readLine()))  {
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;
                                m = element.matcher(line);
                                if (m.matches()) {
                                    String str = m.group(2);
                                    str = str.replace("\"", "");
                                    switch (m.group(1)) {
                                        case "name":
                                            name = str;
                                            break;
                                        case "isoFunction":
                                            iso_function = str;
                                            break;
                                        case "isoScalar":
                                            iso_scalar = str;
                                            break;
                                        case "scalarFunction":
                                            scalar_function = str;
                                            break;
                                        case "scalarScalar":
                                            scalar_scalar = str;
                                            break;
                                        case "legendStyle":
                                            legendStyle = str;
                                            break;
                                        case "isoMin":
                                            iso_min = Double.parseDouble(str); 
                                            activeSim.println(iso_min);
                                            break;
                                        case "isoMax":
                                            iso_max = Double.parseDouble(str); 
                                            activeSim.println(iso_max);
                                            break;
                                        case "scalarMin":
                                            scalar_min = Double.parseDouble(str); 
                                            activeSim.println(scalar_min);
                                            break;
                                        case "scalarMax":
                                            scalar_max = Double.parseDouble(str); 
                                            activeSim.println(scalar_max);
                                            break;
                                        case "setSym":
                                            setSym = Boolean.parseBoolean(str);
                                            break;
                                        default: activeSim.println("Input <<" + line + ">> unreadable");
                                    }
                                }
                                else activeSim.println("Input <<" + line + " >> unreadable");
                            }
                            FieldFunction iso_ff = parseFieldFunction(iso_function, iso_scalar);
                            FieldFunction sc_ff = parseFieldFunction(scalar_function, scalar_scalar);
                            Scene sc = createIsosurfaceScene(iso_ff, iso_min, iso_max, sc_ff, scalar_min, scalar_max, setSym, legendStyle);

                            path = activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_" + iso_function + "_" + scalar_function + File.separator + iso_function + "_" + scalar_function + timeFormat.format(physicalTime);

                            exportStdViews(sc, path);
                            if (keepVis == false) {
                                try {activeSim.getSceneManager().deleteScene(sc);}
                                catch (Exception delIsostd) {}
                            }
                        }
                        break;
                        case "ScalarPic": {
                            double min = 0, max = 100;
                            boolean setSym = false;
                            String name = "", function = "Pressure", scalar = "Magnitude", path = "Eval" , legendStyle = "spectrum";
                            
                            while (null != (line = br.readLine()))  {
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;
                                m = element.matcher(line);
                                if (m.matches()) {
                                    String str = m.group(2);
                                    str = str.replace("\"", "");
                                    switch (m.group(1)) {
                                        case "name":
                                            name = str;
                                            break;
                                        case "function":
                                            function = str;
                                            break;
                                        case "scalar":
                                            scalar = str;
                                            break;
                                        case "legendStyle":
                                            legendStyle = str;
                                            break;
                                        case "min":
                                            min = Double.parseDouble(str);
                                            break;
                                        case "max":
                                            max = Double.parseDouble(str);
                                            break;
                                        case "setSym":
                                            setSym = Boolean.parseBoolean(str);
                                            break;
                                        default: activeSim.println("Input <<" + line + ">> unreadable");
                                    }
                                }
                                else activeSim.println("Input <<" + line + " >> unreadable");
                            }
                            FieldFunction ff = parseFieldFunction(function, scalar);
                            Scene sc = createScalarScene(ff, min, max, setSym, legendStyle);

                            path = activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_" + function + "_" + scalar + File.separator + function + timeFormat.format(physicalTime);

                            exportStdViews(sc, path);
                            if (keepVis == false) {
                                try {activeSim.getSceneManager().deleteScene(sc);}
                                catch (Exception delIsostd) {}
                            }
                        }
                        break;
                        case "IsoScene": {
                            double iso_min = 0, iso_max = 100;
                            double scalar_min = 0, scalar_max = 100;
                            boolean setSym = false;
                            String name = "", iso_function = "Pressure", iso_scalar = "Magnitude", path = "Eval", legendStyle = "spectrum";
                            String scalar_function = "Pressure", scalar_scalar = "Magnitude"; 
                            
                            while (null != (line = br.readLine()))  {
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;
                                m = element.matcher(line);
                                if (m.matches()) {
                                    String str = m.group(2);
                                    str = str.replace("\"", "");
                                    switch (m.group(1)) {
                                        case "name":
                                            name = str;
                                            break;
                                        case "isoFunction":
                                            iso_function = str;
                                            break;
                                        case "isoScalar":
                                            iso_scalar = str;
                                            break;
                                        case "scalarFunction":
                                            scalar_function = str;
                                            break;
                                        case "scalarScalar":
                                            scalar_scalar = str;
                                            break;
                                        case "legendStyle":
                                            legendStyle = str;
                                            break;
                                        case "isoMin":
                                            iso_min = Double.parseDouble(str); 
                                            break;
                                        case "isoMax":
                                            iso_max = Double.parseDouble(str); 
                                            break;
                                        case "scalarMin":
                                            scalar_min = Double.parseDouble(str); 
                                            break;
                                        case "scalarMax":
                                            scalar_max = Double.parseDouble(str); 
                                            break;
                                        case "setSym":
                                            setSym = Boolean.parseBoolean(str);
                                            break;
                                        default: activeSim.println("Input <<" + line + ">> unreadable");
                                    }
                                }
                                else activeSim.println("Input <<" + line + " >> unreadable");
                            }
                            FieldFunction iso_ff = parseFieldFunction(iso_function, iso_scalar);
                            FieldFunction sc_ff = parseFieldFunction(scalar_function, scalar_scalar);
                            Scene sc = createIsosurfaceScene(iso_ff, iso_min, iso_max, sc_ff, scalar_min, scalar_max, setSym, legendStyle);

                            path = activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_" + iso_function + "_" + scalar_function + timeFormat.format(physicalTime);

                            exportScene(sc, path, iso_function);
                            if (keepVis == false) {
                                try {activeSim.getSceneManager().deleteScene(sc);}
                                catch (Exception delIsostd) {}
                            }
                        }
                        break;
                        case "ResampledScene": {
                            double iso_min = 0, iso_max = 100;
                            boolean setSym = false;
                            String name = "", iso_function = "Pressure", iso_scalar = "Magnitude", path = "Eval", legendStyle = "spectrum";
                            
                            while (null != (line = br.readLine()))  {
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;
                                m = element.matcher(line);
                                if (m.matches()) {
                                    String str = m.group(2);
                                    str = str.replace("\"", "");
                                    switch (m.group(1)) {
                                        case "name":
                                            name = str;
                                            break;
                                        case "function":
                                            iso_function = str;
                                            break;
                                        case "scalar":
                                            iso_scalar = str;
                                            break;
                                        case "legendStyle":
                                            legendStyle = str;
                                            break;
                                        case "min":
                                            iso_min = Double.parseDouble(str); 
                                            break;
                                        case "max":
                                            iso_max = Double.parseDouble(str); 
                                            break;
                                        case "setSym":
                                            setSym = Boolean.parseBoolean(str);
                                            break;
                                        default: activeSim.println("Input <<" + line + ">> unreadable");
                                    }
                                }
                                else activeSim.println("Input <<" + line + " >> unreadable");
                            }
                            FieldFunction iso_ff = parseFieldFunction(iso_function, iso_scalar);
                            Scene sc = createResampledVolumeScene(iso_ff, iso_min, iso_max, setSym, legendStyle);

                            path = activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_" + iso_function + "_" + iso_scalar + "_" + timeFormat.format(physicalTime);

                            exportScene(sc, path, iso_function);
                            if (keepVis == false) {
                                try {activeSim.getSceneManager().deleteScene(sc);}
                                catch (Exception delIsostd) {}
                            }
                        }
                        break;
                        case "ScalarScene": {
                            double min = 0, max = 100;
                            boolean setSym = false;
                            String name = "", function = "Pressure", scalar = "Magnitude", path = "Eval" , legendStyle = "blue-red bright";
                            
                            while (null != (line = br.readLine()))  {
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;
                                m = element.matcher(line);
                                if (m.matches()) {
                                    String str = m.group(2);
                                    str = str.replace("\"", "");
                                    switch (m.group(1)) {
                                        case "name":
                                            name = str;
                                            break;
                                        case "function":
                                            function = str;
                                            break;
                                        case "scalar":
                                            scalar = str;
                                            break;
                                        case "min":
                                            min = Double.parseDouble(str);
                                            break;
                                        case "max":
                                            max = Double.parseDouble(str);
                                            break;
                                        case "setSym":
                                            setSym = Boolean.parseBoolean(str);
                                            break;
                                        case "legendStyle":
                                            legendStyle = str;
                                            break;
                                        default: activeSim.println("Input <<" + line + ">> unreadable");
                                    }
                                }
                                else activeSim.println("Input <<" + line + " >> unreadable");
                            }
                            FieldFunction ff = parseFieldFunction(function, scalar);
                            Scene sc = createScalarScene(ff, min, max, setSym, legendStyle);

                            path = activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_" + function + timeFormat.format(physicalTime);

                            exportScene(sc, path, function);
                            if (keepVis == false) {
                                try {activeSim.getSceneManager().deleteScene(sc);}
                                catch (Exception delIsostd) {}
                            }
                        }
                        break;
                        case "Reports": {
                            int iterations = 300;
                            String singleReports = "SingleReports.txt", doubleReports = "DoubleReports.txt";
                            
                            while (null != (line = br.readLine()))  {
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;
                                m = element.matcher(line);
                                if (m.matches()) {
                                    String str = m.group(2);
                                    str = str.replace("\"", "");
                                    switch (m.group(1)) {
                                        case "singleReports":
                                            singleReports = str;
                                            break;
                                        case "doubleReports":
                                            doubleReports = str;
                                            break;
                                        case "iterations":
                                            iterations = Integer.parseInt(str);
                                            break;
                                        default: activeSim.println("Input <<" + line + ">> unreadable");
                                    }
                                }
                                else activeSim.println("Input <<" + line + " >> unreadable");
                            }
                            exportReports(iterations, singleReports, doubleReports);
                        }
                        break;
                        case "Plot": {
                            String monitorName = "Cx_a";
                            double min = 0, max = 4;
                            
                            while (null != (line = br.readLine()))  {
                                if (line.isEmpty()) break;
                                if (line.startsWith("#")) continue;
                                m = element.matcher(line);
                                if (m.matches()) {
                                    String str = m.group(2);
                                    str = str.replace("\"", "");
                                    activeSim.println(str);
                                    switch (m.group(1)) {
                                        case "monitorName":
                                            monitorName = str;
                                            break;
                                        case "min":
                                            min = Double.parseDouble(str);
                                            break;
                                        case "max":
                                            max = Double.parseDouble(str);
                                            break;
                                        default: activeSim.println("Input <<" + line + ">> unreadable");
                                    }
                                }
                                else activeSim.println("Input <<" + line + " >> unreadable");
                            }
                            exportPlot(monitorName, min, max);
                        }
                        break;
                        case "PlotResiduals": {
                            exportResiduals();
                        }
                        break;
                        case "ExportWallYplus": {
                            exportWallYplus();
                        }
                        break;
                    }
                }
                else if (!line.isEmpty()) activeSim.println("Expression does not match.");
            }
            br.close();
        } catch (FileNotFoundException ex3) {
            activeSim.println("Datei mit Einstellungen nicht gefunden!");
        } catch (IOException ex4) {
            activeSim.println("Datei mit Einstellungen konnte nicht geschlossen werden!");
        }
    }

    public void exportResiduals() {
        
        //start timing
        long startTime = System.currentTimeMillis();
        
        // Exportieren des Residual plots
        
        ResidualPlot residualPlot_0 = 
            ((ResidualPlot) activeSim.getPlotManager().getPlot("Residuals"));

        residualPlot_0.open();

        Cartesian2DAxisManager cartesian2DAxisManager_1 = 
            ((Cartesian2DAxisManager) residualPlot_0.getAxisManager());
            
        Cartesian2DAxis cartesian2DAxis_0 = 
            ((Cartesian2DAxis) cartesian2DAxisManager_1.getAxis("Bottom Axis"));

        cartesian2DAxis_0.setMinimum(0.0);
        cartesian2DAxis_0.setMaximum(currentIterations);
            
            Cartesian2DAxis cartesian2DAxis = 
                    ((Cartesian2DAxis) cartesian2DAxisManager_1.getAxis("Left Axis"));

            cartesian2DAxis.setMinimum(1.0E-7);
        cartesian2DAxis.setMaximum(1.0);
            
        residualPlot_0.encode(resolvePath(activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_Residuals.png"), "png", 2000, 1000);
        
        if (keepVis == false) {
            residualPlot_0.close();
        }
        
        //End timing
        long endTime = System.currentTimeMillis();
        double timeTaken =  ((double)(endTime - startTime))/1000;
                
        activeSim.println("Time taken exporting residuals plot: " + timeTaken + "s");
    }
        
        
    public void exportPlot(String plotName, double min, double max) {
        //start timing
        long startTime = System.currentTimeMillis();
        activeSim.println("Exporting plot: " + plotName);
            
        try {
            // Exportieren des cz plots
            MonitorPlot monitorPlot_1 = 
                ((MonitorPlot) activeSim.getPlotManager().getPlot(plotName));

            monitorPlot_1.open();
            
            Cartesian2DAxisManager cartesian2DAxisManager_2 = 
                ((Cartesian2DAxisManager) monitorPlot_1.getAxisManager());

            Cartesian2DAxis cartesian2DAxis_3 = 
                ((Cartesian2DAxis) cartesian2DAxisManager_2.getAxis("Bottom Axis"));

            cartesian2DAxis_3.setMinimum(0.0);
            cartesian2DAxis_3.setMaximum(currentIterations);

            Cartesian2DAxis cartesian2DAxis_4 = 
                ((Cartesian2DAxis) cartesian2DAxisManager_2.getAxis("Left Axis"));

            cartesian2DAxis_4.setMaximum(max);
            cartesian2DAxis_4.setMinimum(min);

            monitorPlot_1.encode(resolvePath(activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_" + plotName + ".png"), "png", 2000, 1000);
                
            if (keepVis == false) {
                monitorPlot_1.close();
            }
        } catch (Exception ex_plot) {activeSim.println("Monitor konnte nicht gefunden werden!");}
        
        //End timing
        long endTime = System.currentTimeMillis();
        double timeTaken =  ((double)(endTime - startTime))/1000;
                
        activeSim.println("Time taken exporting " + plotName +": " + timeTaken + "s");
    }


    public void exportWallYplus() {
        //start timing
        long startTime = System.currentTimeMillis();
            
        // Exportieren des Wall y+ Displayers
        activeSim.println("exporting Wall Y+ images");
            
        //creating DoubleVectors for POV
        DoubleVector focalpoint;
        DoubleVector pointOfView;
        DoubleVector viewUp;
        Double zoom;

        Scene scene_0 = 
            activeSim.getSceneManager().createScene();

        scene_0.initializeAndWait();

        ScalarDisplayer scalarDisplayer_1 = 
            scene_0.getDisplayerManager().createScalarDisplayer("Scalar");

        scalarDisplayer_1.initialize();

        PrimitiveFieldFunction primitiveFieldFunction_0 = 
            ((PrimitiveFieldFunction) activeSim.getFieldFunctionManager().getFunction("WallYplus"));

        scalarDisplayer_1.getScalarDisplayQuantity().setFieldFunction(primitiveFieldFunction_0);

        scalarDisplayer_1.getInputParts().setQuery(null);

        //Zuweisen der Boundaries zum Scalar Displayer
        scalarDisplayer_1.getInputParts().setObjects(carparts);
            
        scene_0.open();

        CurrentView currentView_0 = 
            scene_0.getCurrentView();
            
        //exporting the bottom view
        focalpoint = new DoubleVector(new double[] {.6,-0.4,0});
        pointOfView = new DoubleVector(new double[] {.6,-0.4,-1});
        viewUp = new DoubleVector(new double[] {0,-1,0});
        zoom = 1.25;
        currentView_0.setInput(focalpoint, pointOfView, viewUp, zoom, 1);
            
        //high yplus
        scalarDisplayer_1.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] {30.0, 300.0}));
        scene_0.printAndWait(resolvePath(activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_WallYPlus" + File.separator + activeSim.getPresentationName()
                    + "_Wall_Y_Plus_high_bottom" + timeFormat.format(physicalTime) + ".png"), 1, 2000, 1500, true, false);

        //Buffer yplus
        scalarDisplayer_1.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] {5.0, 30.0}));
        scene_0.printAndWait(resolvePath(activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_WallYPlus" + File.separator + activeSim.getPresentationName()
                    + "_Wall_Y_Plus_buffer_bottom" + timeFormat.format(physicalTime) + ".png"), 1, 2000, 1500, true, false);

        //low yplus
        scalarDisplayer_1.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] {0.0, 5.0}));
        scene_0.printAndWait(resolvePath(activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_WallYPlus" + File.separator + activeSim.getPresentationName()
                    + "_Wall_Y_Plus_low_bottom" + timeFormat.format(physicalTime) + ".png"), 1, 2000, 1500, true, false);
            
            
        //exporting the iso view
        focalpoint = new DoubleVector(new double[] {1,0,0});
        pointOfView = new DoubleVector(new double[] {0,-1,1});
        viewUp = new DoubleVector(new double[] {0,0,1});
        zoom = 1.6;
        currentView_0.setInput(focalpoint, pointOfView, viewUp, zoom, 1);

        //high yplus
        scalarDisplayer_1.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] {30.0, 300.0}));
        scene_0.printAndWait(resolvePath(activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_WallYPlus" + File.separator + activeSim.getPresentationName()
                    + "_Wall_Y_Plus_high_iso" + timeFormat.format(physicalTime) + ".png"), 1, 2000, 1500, true, false);

        //Buffer yplus
        scalarDisplayer_1.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] {5.0, 30.0}));
        scene_0.printAndWait(resolvePath(activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_WallYPlus" + File.separator + activeSim.getPresentationName()
                    + "_Wall_Y_Plus_buffer_iso" + timeFormat.format(physicalTime) + ".png"), 1, 2000, 1500, true, false);

        //low yplus
        scalarDisplayer_1.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] {0.0, 5.0}));
        scene_0.printAndWait(resolvePath(activeSim.getSessionDir() + File.separator + activeSim.getPresentationName() + "_WallYPlus" + File.separator + activeSim.getPresentationName()
                    + "_Wall_Y_Plus_low_iso" + timeFormat.format(physicalTime) + ".png"), 1, 2000, 1500, true, false);
            
        if (keepVis == false) {
            activeSim.getSceneManager().deleteScene(scene_0);
        }

        activeSim.println("done exporting Wall Y+ images");

        //End timing
        long endTime = System.currentTimeMillis();
        double timeTaken =  ((double)(endTime - startTime))/1000;
                
        activeSim.println("Time taken exporting Wall Y+ images: " + timeTaken + "s");
    }
    

    public void exportReports (int gemittelteIterationen, String SingleValuePath, String DoubleValuePath){     
        //start timing
        long startTime = System.currentTimeMillis();
            
        //Settings reports mitteln
        List<String> monitorNamen1 = new ArrayList<String>(); 
        List<String> monitorNamen2 = new ArrayList<String>(); 
        
        // Single Monitors
        try {    
            BufferedReader br = new BufferedReader(new FileReader(activeSim.getSessionDir()+File.separator+SingleValuePath));        
            String line = br.readLine();

            while (line != null) {
                monitorNamen1.add(line);
                activeSim.println(line);
                line = br.readLine();
            } 
            br.close();
        }
        catch(FileNotFoundException ex) {
            activeSim.println("Report-Mittelung Init: Datei " + SingleValuePath + " konnte nicht erstellt/gefunden werden.");
        }
        catch(java.io.IOException ex) {
            activeSim.println ("General IO Exception");
        }
        
        // Double Monitors
        try {    
            BufferedReader br = new BufferedReader(new FileReader(activeSim.getSessionDir()+File.separator+DoubleValuePath));
            String line = br.readLine();

            while (line != null) {
                monitorNamen2.add(line);
                activeSim.println(line);
                line = br.readLine();
            } 
            br.close();
        }
        catch(FileNotFoundException ex) {
            activeSim.println("Report-Mittelung Init: Datei " + DoubleValuePath + " konnte nicht erstellt/gefunden werden.");
        }
        catch(java.io.IOException ex) {
            activeSim.println ("General IO Exception");
        }

        int single_lengh = monitorNamen1.size();
        List<String> monitorNamenZusammen = monitorNamen1;
        monitorNamenZusammen.addAll(monitorNamen2);
        
        double[] reportMonitor_values = new double[gemittelteIterationen];
        double[] addierte_values = new double[monitorNamenZusammen.size()]; // Summe der zu mittelnden Werte je Monitor (in jedem Platz des Arrays wird jeweils die Summe für den entsprechenden Monitor gespeichert)
        ReportMonitor zwischenReportMonitor; // Variable fuer die Schleifen in denen alle y-Werte zwischengespeichert werden.
        
        /*
            Die zwei Schleifen gehen alle Uebergebenen Monitore durch und summieren deren Werte jeweils auf.
            Gespeichert wird in addierteLiftDrag_values, in der uebergebenen Monitorname Reihenfolge
        */
        for(int i = 0; i < monitorNamenZusammen.size(); i++) {
            zwischenReportMonitor = ((ReportMonitor) activeSim.getMonitorManager().getMonitor(monitorNamenZusammen.get(i))); //Erstellt das Monitor-Objekt
            reportMonitor_values = zwischenReportMonitor.getAllYValues(); //Achtung, das Array wird dadurch überschrieben und die Länge neu festgelegt
            addierte_values[i]=0; // Auf 0 setzen. Eigentlich nicht noetig aber zur Sicherheit.
            
            for(int j=reportMonitor_values.length-(gemittelteIterationen) ; j<reportMonitor_values.length; j++) {
                addierte_values[i] += reportMonitor_values[j]; //Summiert mit jedem Schritt den naechsten zu mittelnden Wert auf.
            }
        }
        
        /*
            In diesem Try Block der verschiedene IOException wirft wird die Ausgabe in eine .txt geschrieben
        */
        try 
        {
            File textAuswertung = new File(activeSim.getSessionDir()+File.separator+".."+File.separator + "Reports.txt");
            
            // Angabe des Speicherort im Output und anschliessend bei der Definition des FileWriter
            FileWriter fw = new FileWriter(textAuswertung, true);
            BufferedWriter bw = new BufferedWriter(fw);
            
            //Schreibt erste Zeile wenn die Datei leer ist.
            
            if(textAuswertung.length() == 0)
            {
                bw.write("Simulation" + "\t"); 
                for(String monitorNameLiftDrag :monitorNamenZusammen)
                {
                    bw.write(monitorNameLiftDrag + "\t");
                }
            }
            
            bw.newLine(); //Neue Zeile
            
            bw.write(activeSim.getPresentationName() + "\t");
            
            //Ausgabe zweite Zeile: 
            for(int i = 0; i< addierte_values.length; i++)
            {
                if (i >= single_lengh ) addierte_values[i] *= 2; // Werte werden mal 2 genommen. Symmetriebedingung.
                addierte_values[i] = addierte_values[i]/gemittelteIterationen; // Mittelung der Werte
                bw.write(addierte_values[i] + "\t");
            }
            bw.close();
        }
        catch(FileNotFoundException ex)
        {
            System.err.println("Report-Mittelung: Datei konnte nicht erstellt/gefunden werden.");
        }
        catch(IOException ex)
        {
            System.err.println("Ausgabe-Fehler");
        }

        //End timing
        long endTime = System.currentTimeMillis();
        double timeTaken =  ((double)(endTime - startTime))/1000;
                    
        activeSim.println("Time taken writing reports: " + timeTaken + "s");
    }


	public void createPlane(String name, String ffString, double startOffset, double planeOffset, int numPlanes) {

		Units units_0 = 
		  ((Units) activeSim.getUnitsManager().getObject("m"));
		
		//creating planes
		PlaneSection planeSection_1 = 
		 (PlaneSection) activeSim.getPartManager().createImplicitPart(
                 new NeoObjectVector(new Object[] {}),
                 new DoubleVector(new double[] {startOffset, 0.0, 0.0}),
                 new DoubleVector(new double[] {1.0, 0.0, 0.0}),
                 0,
                 1,
                 new DoubleVector(new double[] {0.0}));
		
		//assigning regions 
		planeSection_1.getInputParts().setQuery(null);
		planeSection_1.getInputParts().setObjects(regions);
		
		
		//Get coordinate system for usage in the for-loop
		Coordinate coordinate_0 = planeSection_1.getOriginCoordinate();
		
		//creating export table fro usage in the for-loop, assigning Velocity as export parameter
		XyzInternalTable xyzInternalTable_1 = activeSim.getTableManager().createTable(XyzInternalTable.class);
		
		PrimitiveFieldFunction primitiveFieldFunction_0 = ((PrimitiveFieldFunction) activeSim.getFieldFunctionManager().getFunction(ffString));
		VectorMagnitudeFieldFunction vectorMagnitudeFieldFunction_0 = ((VectorMagnitudeFieldFunction) primitiveFieldFunction_0.getMagnitudeFunction());
		VectorComponentFieldFunction vectorComponentFieldFunction_0 = ((VectorComponentFieldFunction) primitiveFieldFunction_0.getComponentFunction(0));
		VectorComponentFieldFunction vectorComponentFieldFunction_1 = ((VectorComponentFieldFunction) primitiveFieldFunction_0.getComponentFunction(1));
		VectorComponentFieldFunction vectorComponentFieldFunction_2 = ((VectorComponentFieldFunction) primitiveFieldFunction_0.getComponentFunction(2));
		xyzInternalTable_1.setFieldFunctions(new NeoObjectVector(new Object[] {vectorMagnitudeFieldFunction_0, vectorComponentFieldFunction_0, vectorComponentFieldFunction_1, vectorComponentFieldFunction_2}));
		
		//Identifiing File Path
		String path = activeSim.getSessionDir();
		
		//planeSection_1.setValueMode(ValueMode.RANGE);
		
		for(int i=0; i<numPlanes; i++){
			
            String zaehlerString = addLeadingZeros(i, 3);

			coordinate_0.setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {startOffset+i*planeOffset, 0.0, 0.0}));
			
			xyzInternalTable_1.getParts().setObjects(planeSection_1);
			xyzInternalTable_1.extract();
			xyzInternalTable_1.export(path + File.separator +  ffString + "_"  + timeFormat.format(physicalTime) + "_" + zaehlerString + ".csv", ",");
		}

	}
    
	public void planeExport (String name, String view_name, FieldFunction fieldFunction_0, double scaleMin, double scaleMax, double stepSize, double startCoordinate, double endCoordinate, DoubleVector focalpoint, DoubleVector pointOfView, double zoom, boolean showMesh){
        
        /*
        Diese Methode exportiert eine Serie von Bildern einer schrittweise verschobenen Schnittebene mit einer Skalaren Funktion.
        Die Blickrichtung ist dabei immer senkrecht zur Ebene. Die Verschiebung der Ebene findet ebenfalls in dieser Richtung statt.
        Schrittweite, Start- und Endwert werden über Parameter vorgegeben.
        
        This method exports a series of images of a plane section with a scalar displayer.
        The POV and viewing direction are defined as parameters. The created sections are always perpendicular to the viewing direction.
        Also, the offset shift of the plane is always done in this direction.
        Offset coordinates and step size for shift offset are defined as parameters.
        Since the plane is created at the coordinate origin, all offsets refer to this origin.
        */
        
        //start timing
        long startTime = System.currentTimeMillis();
        
        activeSim.println("exporting planes");
        
        //calculating viewing direction from difference of focalpoint and point of view
        DoubleVector ViewDirection = new DoubleVector(new double[pointOfView.size()]);
            for (int i = 0; i<pointOfView.size(); i++){
              ViewDirection.setComponent(i, focalpoint.getComponent(i)-pointOfView.getComponent(i));
            }          
        
        DoubleVector ViewDirectionNormalized = normalize(ViewDirection); //normalizing the view direction vector to be able to scale it later on.
        activeSim.println("Blickrichtung: " + ViewDirection.toString());
        activeSim.println("Normalisierte Blickrichtung: " + ViewDirectionNormalized.toString());
        
        
        //Creating the plane section in model origin with 
        PlaneSection planeSection_TP = 
          (PlaneSection) activeSim.getPartManager().createImplicitPart(new NeoObjectVector(new Object[] {}), ViewDirection, new DoubleVector(new double[] {0.0, 0.0, 0.0}), 0, 1, new DoubleVector(new double[] {0.0}));

        planeSection_TP.setPresentationName("total pressure section");
        
        //defining all regions and boundaries and assinging them to the plane section
        planeSection_TP.getInputParts().setObjects(regions);
        
        //creating the scene
        Scene scene_0 = activeSim.getSceneManager().createScene();
        scene_0.initializeAndWait();
        SceneUpdate sceneUpdate_0 = scene_0.getSceneUpdate();
        
        //scene settings
        scene_0.setBackgroundColorMode(BackgroundColorMode.SOLID);
        
        SolidBackgroundColor solidBackgroundColor_0 = 
            scene_0.getSolidBackgroundColor();

        solidBackgroundColor_0.setColor(new DoubleVector(new double[] {0.4666999876499176, 0.53329998254776, 0.6000000238418579}));
        
        //creating a scalar displayer
        ScalarDisplayer scalarDisplayer_0 = 
          scene_0.getDisplayerManager().createScalarDisplayer("Scalar");

        //legend settings
        Legend legend_0 =
          scalarDisplayer_0.getLegend();
        
        SpectrumLookupTable spectrumLookupTable_0 = 
          ((SpectrumLookupTable) activeSim.get(LookupTableManager.class).getObject("spectrum"));

        legend_0.setLookupTable(spectrumLookupTable_0);        
        legend_0.setLevels(64);
		legend_0.setHeight(0.01);
		legend_0.setTitleHeight(0.024);
		legend_0.setLabelHeight(0.018);
		
        //assigning the plane section to the scalar displayer
        scalarDisplayer_0.initialize();
        scalarDisplayer_0.getInputParts().setQuery(null);
        scalarDisplayer_0.getInputParts().setObjects(planeSection_TP);

        // assigning fieldFunction to displayer
        scalarDisplayer_0.getScalarDisplayQuantity().setFieldFunction(fieldFunction_0);
		
        // set the scale
        scalarDisplayer_0.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] {scaleMin, scaleMax}));

        addAnnotations(scene_0);

        CurrentView currentView_0 = scene_0.getCurrentView();
        currentView_0.setInput(focalpoint, pointOfView, new DoubleVector(new double[] {0.0, 0.0, 1.0}), zoom, 1);
        
        //setting the displayer properties
        if (showMesh == true) {
                scalarDisplayer_0.setDisplayMesh(1);
        }
        scalarDisplayer_0.setFillMode(ScalarFillMode.NODE_FILLED);
        scalarDisplayer_0.getScalarDisplayQuantity().setClip(ClipMode.NONE);

        
        int zaehler=1; //counter for file names to sort the files by their names in correct order
        
        //in the following loop the plane origin is set off to "a", which ranges from the user defined start to end Coordinates and increases by step size.
        //for each position an image is exported
        for (double a=startCoordinate;a<endCoordinate;a=a+stepSize){
            
            String zaehlerString = addLeadingZeros(zaehler, 3); //adding leading zeros to the file counter to make the windows explorer sort the files in the correct order.
            
            //set the origin of the plane section to the pointOfViewNormalized vector scaled with "a".
            DoubleVector PlaneOrigin = new DoubleVector(new double[] {ViewDirectionNormalized.getComponent(0)*a,ViewDirectionNormalized.getComponent(1)*a,ViewDirectionNormalized.getComponent(2)*a});;
            planeSection_TP.setOrigin(PlaneOrigin);
            
            //printing the scene to an image file
                scene_0.printAndWait(resolvePath(activeSim.getSessionPath() + name + File.separator + view_name + "_" + timeFormat.format(physicalTime) + "_"  +  zaehlerString + "PlaneOffset" + Math.round( a * 100 ) / 100.0 + ".png"), 1, 1800, 1200, true, false);
            zaehler++;
        }   
        
        //deleting created scene and plane
        if (keepVis == false){
            activeSim.getPartManager().deletePart(planeSection_TP);
            activeSim.getSceneManager().deleteScene(scene_0);
        }
        activeSim.println("done exporting planes");
        
        //End timing
        long endTime = System.currentTimeMillis();
        double timeTaken =  ((double)(endTime - startTime))/1000;

        activeSim.println("Time taken exporting planes: " + timeTaken + "s");
    }
	
    
    // -----------------------
    // SCENE CREATOR FUNCTIONS
    // -----------------------
    public void addAnnotations(Scene scene_0) {

        PhysicalTimeAnnotation physicalTimeAnnotation_0 = 
          ((PhysicalTimeAnnotation) activeSim.getAnnotationManager().getObject("Solution Time"));

        PhysicalTimeAnnotationProp physicalTimeAnnotationProp_0 = 
          (PhysicalTimeAnnotationProp) scene_0.getAnnotationPropManager().createPropForAnnotation(physicalTimeAnnotation_0);

        TimeStepAnnotation timeStepAnnotation_0 = 
          ((TimeStepAnnotation) activeSim.getAnnotationManager().getObject("Time Step"));

        SolutionStateAnnotationProp solutionStateAnnotationProp_0 = 
          (SolutionStateAnnotationProp) scene_0.getAnnotationPropManager().createPropForAnnotation(timeStepAnnotation_0);

        solutionStateAnnotationProp_0.setPosition(new DoubleVector(new double[] {0.0, 0.05, 0.0}));
    }

    public Scene createScalarScene(FieldFunction thisFieldFunction, double scaleMin, double scaleMax, boolean setSym, String legendStyle){
        //start timing
        long startTime = System.currentTimeMillis();
        
        activeSim.println("creating Scalar");

        //creating Scene
        Scene thisScene = 
          activeSim.getSceneManager().createScene();

        thisScene.initializeAndWait();
        
        //creating a scalar displayer
        ScalarDisplayer scalarDisplayer_0 = 
            thisScene.getDisplayerManager().createScalarDisplayer("Scalar");

        //assigning the plane section to the scalar displayer
        scalarDisplayer_0.initialize();
        scalarDisplayer_0.getInputParts().setQuery(null);
        scalarDisplayer_0.getInputParts().setObjects(carparts);

        //assigning fieldFunction to displayer and set the scale
        scalarDisplayer_0.getScalarDisplayQuantity().setFieldFunction(thisFieldFunction);
        scalarDisplayer_0.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] {scaleMin, scaleMax}));
        
        if (setSym == true) {
            //Setting Symmetry
            SymmetricRepeat symmetricRepeat_1 = 
                ((SymmetricRepeat) activeSim.getTransformManager().getObject("fluid.Windtunnel.sym 1"));

            scalarDisplayer_0.setVisTransform(symmetricRepeat_1);
        }


        //setting the displayer properties
        scalarDisplayer_0.setFillMode(ScalarFillMode.NODE_FILLED);
        scalarDisplayer_0.getScalarDisplayQuantity().setClip(ClipMode.NONE);
        
        //Setting Legend Style
        Legend legend_0 = scalarDisplayer_0.getLegend();
                
        LookupTable LookupTable_0 = 
            (activeSim.get(LookupTableManager.class).getObject(legendStyle));
                
            /* legendStyle for example
                "blue-red bright"
                "spectrum"
            */

        
        legend_0.setLookupTable(LookupTable_0);

        addAnnotations(thisScene);
        
        activeSim.println("done creating Scalar");
        
        //End timing
        long endTime = System.currentTimeMillis();
        double timeTaken =  ((double)(endTime - startTime))/1000;

        activeSim.println("Time taken creating Scalar: " + timeTaken + "s");
        return thisScene;
    }
    

    public Scene createResampledVolumeScene(FieldFunction fieldFunction, double scaleMin, double scaleMax, boolean setSym, String legendStyle){
        
        //start timing
        long startTime = System.currentTimeMillis();
        
        activeSim.println("exporting Resampled Volume");
        
        Scene scene_0 = activeSim.getSceneManager().createScene();

        //Scene Settings
        scene_0.setBackgroundColorMode(BackgroundColorMode.SOLID);
        
        SolidBackgroundColor solidBackgroundColor_0 = 
            scene_0.getSolidBackgroundColor();

        solidBackgroundColor_0.setColor(new DoubleVector(new double[] {1, 1, 1}));        
        
        scene_0.initializeAndWait();



        Units units_1 = 
          activeSim.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

        Units units_0 = 
          activeSim.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

        PartDisplayer partDisplayer_1 = 
          ((PartDisplayer) scene_0.getCreatorDisplayer());

        partDisplayer_1.initialize();

        scene_0.getCreatorGroup().setQuery(null);
        scene_0.getCreatorGroup().setObjects(regions);

        ScalarDisplayer scalarDisplayer_0 = 
          scene_0.getDisplayerManager().createScalarDisplayer("Resampled Volume Scalar");
        
        scalarDisplayer_0.initialize();
        
        activeSim.println("check1");
        
        ResampledVolumePart resampledVolumePart_1 = 
          activeSim.getPartManager().createResampledVolumePart(new NeoObjectVector(regions.toArray()), 0.1, units_0, 0.004, units_0, units_0, units_0, new DoubleVector(new double[] {2.5663284580761103, -0.5489359277335215, 0.5268039417642574}), units_0, units_0, units_0, new DoubleVector(new double[] {6.781206952869266, 1.5888072779891433, 1.9216290123912447}), units_0, units_0, units_0, new DoubleVector(new double[] {0.0, 0.0, 1.0}), units_1, 0.0);

        activeSim.println("check2");
        
        scalarDisplayer_0.getVisibleParts().addParts(resampledVolumePart_1);

        scene_0.setTransparencyOverrideMode(SceneTransparencyOverride.USE_DISPLAYER_PROPERTY);

        scalarDisplayer_0.getScalarDisplayQuantity().setClip(ClipMode.BOTH);
        scalarDisplayer_0.getScalarDisplayQuantity().setFieldFunction(fieldFunction);
        scalarDisplayer_0.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] {scaleMin, scaleMax}));

        addAnnotations(scene_0);
        
        //End timing
        long endTime = System.currentTimeMillis();
        double timeTaken =  ((double)(endTime - startTime))/1000;

        activeSim.println("Time taken creating Resampled Volume: " + timeTaken + "s");
        return scene_0;
    }

    public Scene createIsosurfaceScene(FieldFunction isoFieldFunction, double isoMin, double isoMax, FieldFunction scalarFieldFunction, double scalarMin, double scalarMax,boolean setSym, String  legendStyle){
        //start timing
        long startTime = System.currentTimeMillis();

        activeSim.println("Creating Isosurface");

        Scene scene_0 = activeSim.getSceneManager().createScene();

        //Scene Settings
        scene_0.setBackgroundColorMode(BackgroundColorMode.SOLID);
        
        SolidBackgroundColor solidBackgroundColor_0 = 
            scene_0.getSolidBackgroundColor();

        solidBackgroundColor_0.setColor(new DoubleVector(new double[] {1, 1, 1}));        
        
        scene_0.initializeAndWait();


        // CREATE PART DISPLAYER
        PartDisplayer partDisplayer_0 = 
          scene_0.getDisplayerManager().createPartDisplayer("Geometry", -1, 4);

        partDisplayer_0.initialize();
        partDisplayer_0.getInputParts().setQuery(null);
        partDisplayer_0.getInputParts().setObjects(carparts);
        partDisplayer_0.setSurface(true);
        partDisplayer_0.setOutline(false);

        // CREATE SCALAR DISPLAYER
        ScalarDisplayer scalarDisplayer_0 = 
          scene_0.getDisplayerManager().createScalarDisplayer("Surface Scalar");
        
        scalarDisplayer_0.initialize();

        Legend legend_0 = 
          scalarDisplayer_0.getLegend();

        BlueRedLookupTable blueRedLookupTable_0 = 
          ((BlueRedLookupTable) activeSim.get(LookupTableManager.class).getObject("blue-red"));

        legend_0.setLookupTable(blueRedLookupTable_0);

        // CREATE SURFACE THRESHOLD
        IsoPart isoPart_0 = 
                  activeSim.getPartManager().createIsoPart(new NeoObjectVector(new Object[] {}), isoFieldFunction);

        isoPart_0.setMode(IsoMode.ISOVALUE_SINGLE);
        // isoPart_0.getInputParts().setObjects(new NeoObjectVector(regions.toArray()));
        isoPart_0.getInputParts().setObjects(regions);

        SingleIsoValue singleIsoValue_0 = isoPart_0.getSingleIsoValue();
        singleIsoValue_0.getValueQuantity().setValue(isoMax);

        scalarDisplayer_0.getVisibleParts().addParts(isoPart_0);
        scalarDisplayer_0.getScalarDisplayQuantity().setClip(ClipMode.MAX);
        scalarDisplayer_0.getScalarDisplayQuantity().setFieldFunction(scalarFieldFunction);
        scalarDisplayer_0.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] {scalarMin, scalarMax}));

        addAnnotations(scene_0);

        //End timing
        long endTime = System.currentTimeMillis();
        double timeTaken =  ((double)(endTime - startTime))/1000;
        activeSim.println("Time taken creating Isosurface: " + timeTaken + "s");
        return scene_0;
    }


    // -----------------------
    // SCENE EXPORT FUNCTIONS
    // -----------------------
    public void exportScene(Scene scene_0, String path, String function){
        
        //start timing
        long startTime = System.currentTimeMillis();
        
        activeSim.println("exporting scene");

        //exporting scene
        scene_0.export3DSceneFileAndWait(resolvePath(path + ".sce"), "sce" ,  "", false, false);

        activeSim.println("done exporting scene");
        
        //end timing
        long endTime = System.currentTimeMillis();
        double timeTaken =  ((double)(endTime - startTime))/1000;

        activeSim.println("Time taken exporting scene: " + timeTaken + "s");
    }
    
    public void exportStdViews(Scene scene_0, String path){
        
        /*
        this method exports .png files of a given Scene in standard views to a path.
        */
        
        CurrentView currentView_0 = 
          scene_0.getCurrentView();
        
        //creating DoubleVectors for POV
        DoubleVector focalpoint;
        DoubleVector pointOfView;
        DoubleVector viewUp;
        Double zoom;
        
        for (int i=0; i<numViews; i++)
        {
            currentView_0.setInput(focalpoints.get(i), pointsOfView.get(i), viewsUp.get(i), viewsZoom.get(i), 1);
            scene_0.printAndWait(resolvePath(path + "_" + viewsName.get(i) + ".png"), 1, 1500, 1500, true, false);
        }
/*
        //exporting the front view
        focalpoint = new DoubleVector(new double[] {0,0,0.3});
        pointOfView = new DoubleVector(new double[] {-1,0,0.3});
        viewUp = new DoubleVector(new double[] {0,0,1});
        zoom = 1.2;
        currentView_0.setInput(focalpoint, pointOfView, viewUp, zoom, 1);
        
        scene_0.printAndWait(resolvePath(path + "_front.png"), 1, 1500, 1500, true, false);

        //exporting the back view
        focalpoint = new DoubleVector(new double[] {0,0,0.3});
        pointOfView = new DoubleVector(new double[] {1,0,0.3});
        viewUp = new DoubleVector(new double[] {0,0,1});
        zoom = 1.2;
        currentView_0.setInput(focalpoint, pointOfView, viewUp, zoom, 1);

        scene_0.printAndWait(resolvePath(path + "_back.png"), 1, 1500, 1500, true, false);

        //exporting the side view
        focalpoint = new DoubleVector(new double[] {1,0,0.3});
        pointOfView = new DoubleVector(new double[] {1,-1,0.3});
        viewUp = new DoubleVector(new double[] {0,0,1});
        zoom = 2.0;
        currentView_0.setInput(focalpoint, pointOfView, viewUp, zoom, 1);
        
        scene_0.printAndWait(resolvePath(path + "_side.png"), 1, 1500, 1500, true, false);

        //exporting the top view
        focalpoint = new DoubleVector(new double[] {1,0,0});
        pointOfView = new DoubleVector(new double[] {1,0,1});
        viewUp = new DoubleVector(new double[] {0,1,0});
        zoom = 2.0;
        currentView_0.setInput(focalpoint, pointOfView, viewUp, zoom, 1);
        
        scene_0.printAndWait(resolvePath(path + "_top.png"), 1, 1500, 1500, true, false);

        //exporting the bottom view
        focalpoint = new DoubleVector(new double[] {1,0,0});
        pointOfView = new DoubleVector(new double[] {1,0,-1});
        viewUp = new DoubleVector(new double[] {0,-1,0});
        zoom = 2.0;
        currentView_0.setInput(focalpoint, pointOfView, viewUp, zoom, 1);

        scene_0.printAndWait(resolvePath(path + "_bottom.png"), 1, 1500, 1500, true, false);

        //exporting the iso view
        focalpoint = new DoubleVector(new double[] {1,0,0});
        pointOfView = new DoubleVector(new double[] {0,-1,1});
        viewUp = new DoubleVector(new double[] {0,0,1});
        zoom = 2.0;
        currentView_0.setInput(focalpoint, pointOfView, viewUp, zoom, 1);

        scene_0.printAndWait(resolvePath(path + "_iso.png"), 1, 1500, 1500, true, false);

        //exporting the iso view
        focalpoint = new DoubleVector(new double[] {0.1,-0.7,0.2});
        pointOfView = new DoubleVector(new double[] {0.3,-0.7,-0.4});
        viewUp = new DoubleVector(new double[] {0,0,1});
        zoom = 0.5;
        currentView_0.setInput(focalpoint, pointOfView, viewUp, zoom, 1);

        scene_0.printAndWait(resolvePath(path + "_iso_wake.png"), 1, 1500, 1500, true, false);

        //exporting the iso view
        focalpoint = new DoubleVector(new double[] {0,-0.55,0});
        pointOfView = new DoubleVector(new double[] {-0.4,-0.4,0.2});
        viewUp = new DoubleVector(new double[] {0,0,1});
        zoom = 0.5;
        currentView_0.setInput(focalpoint, pointOfView, viewUp, zoom, 1);

        scene_0.printAndWait(resolvePath(path + "_iso_tyre.png"), 1, 1500, 1500, true, false);
        */
    }
    

    // ------------------------
    // HELPFUL HELPER FUNCTIONS
    // ------------------------
    public static String addLeadingZeros(int number, int digits){

        /*
        This method adds leading zeros to an integer 'number' and returns it as String with the length of 'digits'.
        Useful as counter in the beginning of file names.
        */
                
        String zeros = "";
        for (int n=1; n <= (digits-String.valueOf(number).length()); n++){
            zeros = zeros + "0";
            }
        String numberString = zeros + number;
        return numberString;
    }

    public static DoubleVector normalize(DoubleVector V){
        
        /*
        this method normalizes DoubleVector V
        */
        
        //determining the length of V
        double vLength = 0;
        for (int i=0; i<V.size(); i++){
            vLength = vLength + Math.pow(V.getComponent(i), 2);
            }
        vLength = Math.sqrt(vLength);

        //normalizing Viewdirection
        DoubleVector VNormalized = new DoubleVector(new double[V.size()]);
        for (int i=0; i<V.size(); i++){
            VNormalized.setComponent(i, V.getComponent(i)/vLength);
        }
        return VNormalized;
    }
    private FieldFunction parseFieldFunction (String function, String scalar) {
        //importing the fieldFunction
        FieldFunction primitiveFieldFunction_0 = 
          ((FieldFunction) activeSim.getFieldFunctionManager().getFunction(function));
        FieldFunction vectorMagnitudeFieldFunction_0;

        if (primitiveFieldFunction_0.getTypeOption().getType().hasVectorStorageType() == true) {
            activeSim.println("Function " + function + " has vector type");
            // if (scalar == "Magnitude") {
                vectorMagnitudeFieldFunction_0 = ((FieldFunction) primitiveFieldFunction_0.getMagnitudeFunction());
            // }
            // else if (scalar == "i") {}
            // else if (scalar == "j") {}
            // else if (scalar == "k") {}
        } else {
            activeSim.println("Function " + function + " has scalar type");
            vectorMagnitudeFieldFunction_0 = ((FieldFunction) primitiveFieldFunction_0);
        }
        return vectorMagnitudeFieldFunction_0;
    }
    
    private List<PartSurface> getSurfaces(String name, String part){
        Simulation simulation = getActiveSimulation();
        MeshOperationPart fluid_meshOperationPart = ((MeshOperationPart) simulation.get(SimulationPartManager.class).getPart(part));
        Collection<PartSurface> All_partSurface = fluid_meshOperationPart.getPartSurfaceManager().getPartSurfaces();
        List<PartSurface> UniteParts = new ArrayList<PartSurface>();
        for (PartSurface forReportSurface : All_partSurface) {
          String nameForReportSurface = forReportSurface.getPresentationName();
          //Teil vom car
          if (nameForReportSurface.startsWith(name)) {
            UniteParts.add(forReportSurface);
          }
        }
        return UniteParts;
    }
}

